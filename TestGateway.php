<?php
require './vendor/autoload.php';
use \WBuilder\Core\Builder;
use \WBuilder\Core\Enums\Mode;

$websiteBuilder = new Builder();
$websiteBuilder->setMode(Mode::LIVE);
$websiteBuilder->setAuthorizeKey("key");
print_r($websiteBuilder->getParameters());
echo "\n";