<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\Price;

class ProductPrice extends Model
{
    public $id;
    public $product_id;
    public Currency $currency;
    public float $price;
    public $discount_type;
    public float $discount;
    public float $tax;
    public ?Price $summary;
    public $sort;
    public $created_at;
    public $updated_at;

    public function init($data){
        $this->summary = $this->total_price();
    }


    protected function total_price() : Price{
        $result = array(
            "sub_total" => $this->price,
            "discount" => $this->discount,
            "tax" => $this->tax,
            "total_price" => 0
        );
        if($this->discount > 0){
            if($this->discount_type == "FIXED"){
                $result['discount'] = $this->discount;
            }else{
                $result['discount'] = (($result['total_price'] * $this->discount) / 100);
            }
            $result['total_price'] = $result['sub_total'] - $result['discount'];
        }
        $result['total_price'] = $result['total_price'] + $result['tax'];
        return new Price($result);
    }

}
