<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Builder;
use WBuilder\Core\Types\ListOfOrderCartItem;

class Order extends Model
{
    public $id;
    public $order_id;
    public $sub_total_price;
    public $shipping_price;
    public $coupon_price;
    public $total_price;
    public $status;
    public ?Currency $currency;
    public ?Address $shipping_address;
    public ?Address $billing_address;
    public $shipping_service;
    public ?ListOfOrderCartItem $cart;
    public $created_at;
    public $updated_at;

}
