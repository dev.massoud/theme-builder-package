<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfMeta;

class DraftMetaData extends Model
{
    /** @var ListOfMeta $meta */
    public ListOfMeta $meta;
    /** @var TemplateGroup[] $groups */
    public $groups;
    /** @var Provider[] $providers */
    public $providers;
    public $theme_url;
    /** @var Currency[] $currencies **/
    public $currencies;
    /** @var Language[] $languages **/
    public $languages;

    public function init($data){
        $this->currencies = builder_model_of_data($data['currency_data'], Currency::class);
        $this->languages = builder_model_of_data($data['language_data'], Language::class);
        $this->providers = builder_model_of_data($data['providers'], Provider::class);
        $this->groups = builder_model_of_data($data['groups'], TemplateGroup::class);
    }

}
