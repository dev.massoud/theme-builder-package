<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Builder;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfService;
use WBuilder\Core\Types\ListOfSocial;

class Meta extends Model
{
    public $id;
    public ?TemplateGroup $group;
    public $order;
    public $title;
    public $meta_key;
    public $meta_type;
    public $conditions;
    public $created_at;
    public $updated_at;
    public $data;

    public function init($data){

        if($this->meta_type == 'post')
            $this->data = builder_list_of_data($data['posts'], ListOfArticles::class, Article::class);
        elseif ($this->meta_type == 'product')
            $this->data = builder_list_of_data($data['products'], ListOfProduct::class, Product::class);
        elseif ($this->meta_type == 'categories')
            $this->data = builder_list_of_data($data['categories'], ListOfCategories::class, Category::class);
        elseif ($this->meta_type == 'social')
            $this->data = builder_list_of_data($data['meta_value'], ListOfSocial::class, Social::class);
        elseif ($this->meta_type == 'service')
            $this->data = builder_list_of_data($data['services'], ListOfService::class, Service::class);
        elseif ($this->meta_type == 'pricing')
            $this->data = $data['pricing'];
        elseif ($this->meta_type == 'form')
            $this->data = $data['form'];
        elseif ($this->meta_type == 'text' || $this->meta_type == 'textarea' || $this->meta_type == 'tags')
            $this->data = builder_translation($data['meta_value']['value']);
        elseif ($this->meta_type == 'image')
            $this->data = ($data['meta_value']['url']);
        else
            $this->data = $data['meta_value'];
    }

}
