<?php
namespace WBuilder\Core\Models;

class ProductSize extends Model
{
    public $id;
    public $product_id;
    public $size;
    public $stock;
    public $order;
    public $created_at;
    public $updated_at;

}
