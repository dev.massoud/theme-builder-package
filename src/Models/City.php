<?php
namespace WBuilder\Core\Models;

class City extends Model
{
    public $id;
    public $name;
    public ?Country $country;
    public ?State $state;

}
