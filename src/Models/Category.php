<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfCategories;

class Category extends Model
{
    public $id;
    public $category_id;
    public $code;
    public $title;
    public $description;
    public ?ListOfCategories $children;
    public $sort;
    public $created_at;
    public $updated_at;

    public function init($data){
        $category = isset($data['category'])?$data['category'][0]:$data;
        $this->code = $category['code'];
        $this->sort = $category['sort'];
        $this->title = builder_translate($category['translations'], 'title');
        $this->description = builder_translate($category['translations'], 'description');
        $this->children = builder_list_of_data($category['children'], ListOfCategories::class, Category::class);
    }
}
