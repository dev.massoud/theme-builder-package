<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfAddress;
class Customer extends AuthModel
{
    public $id;
    public $email;
    public $firstname;
    public $lastname;
    public $gender;
    public $phone_number;
    public $avatar;
    public $favorite_count;
    public ?ListOfAddress $billing_addresses;
    public ?ListOfAddress $shipping_addresses;

}
