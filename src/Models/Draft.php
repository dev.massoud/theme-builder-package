<?php
namespace WBuilder\Core\Models;

class Draft extends Model
{
    public $id;
    public $user_id;
    public $job_id;
    public $template_id;
    public $code;
    public $job_describe;
    public $business_title;
    public ?DraftMetaData $meta_data;
    public ?Template $template;
    public $created_at;
    public $updated_at;

    public function init($data){

    }
}
