<?php
namespace WBuilder\Core\Models;

use Illuminate\Support\Facades\Auth;
use WBuilder\Core\Builder;
use WBuilder\Core\Types\ListOfProductMedia;
use WBuilder\Core\Types\ListOfProductSize;
use WBuilder\Core\Types\ListOfProductPrice;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfCategories;

class Product extends Model
{
    public $id;
    public $code;
    public $stock;
    public $rating;
    public $ratings;
    public $status;
    public $additional_data;
    public $created_at;
    public $updated_at;
    public $title;
    public $short_content;
    public $long_content;
    public $seo_keywords;
    public ?ProductPrice $price;
    public $image;
    public ?ListOfProductMedia $media;
    public ?ListOfProductPrice $prices;
    public ?ListOfCategories $categories;
    public ?ListOfProductSize $sizes;
    public ?ListOfProductColor $colors;

    public function init($data){
        if(isset($data['translations'])){
            $this->title = builder_translate($data['translations'], 'title');
            $this->short_content = builder_translate($data['translations'], 'short_content');
            $this->long_content = builder_translate($data['translations'], 'long_content');
            $this->seo_keywords = builder_translate($data['translations'], 'seo_keywords');
        }
        $this->price = builder_default_price($this->prices);
        $this->image = ($this->media->count() > 0)?$this->media->first()->url:"";
    }

    /**
     * check $value in categories title
     * @param $value keyword for check name of category
     * @return |null
     */
    public function has_category($value){
        $result = $this->categories->search($value, 'code');
        if($result)
            return $result->title;
        return null;
    }

    /**
     * return list of category name as string split by comma
     * @return string
     */
    public function categories(){
        $categories = [];
        /** @var Category $category */
        foreach ($this->categories as $category){
            $categories[] = $category->title;
        }
        return implode(", ", $categories);
    }

    /**
     * return product title as slug split by dash for url
     * @return string
     */
    public function slug(){
        return \Illuminate\Support\Str::slug($this->title, "-");
    }

    public function is_favorite(){
        if(!Auth::check()) return false;
        try{
            if($favorite = \builder()->checkFavorite($this->id, Auth::user()->getAuthIdentifier())){
                return $favorite->id != null;
            }

        }catch (\Exception $exception){

        }
        return false;
    }

    /**
     * return product detail url
     * @return string
     */
    public function url($color_id = null, $size_id = null){
        $params = ['pid' => $this->id, 'title' => $this->slug()];
        if($color_id) $params['cid'] = $color_id;
        if($size_id) $params['sid'] = $size_id;
        return route('product', route_params($params));
    }


    function toJson() {
        return json_encode(array(
            "id" => $this->id,
            "code" => $this->code,
            "stock" => $this->stock,
            "rating" => $this->rating,
            "ratings" => $this->ratings,
            "status" => $this->status,
            "additional_data" => $this->additional_data,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "title" => $this->title,
            "short_content" => $this->short_content,
            "long_content" => $this->long_content,
            "seo_keywords" => $this->seo_keywords,
            "price" => ($this->price)?$this->price->toJson():null,
            "image" => $this->image,
            "media" => ($this->media)?$this->media->toJson():null,
            "prices" => ($this->prices)?$this->prices->toJson():null,
            "categories" => ($this->categories)?$this->categories->toJson():null,
            "sizes" => ($this->sizes)?$this->sizes->toJson():null,
            "colors" => ($this->colors)?$this->colors->toJson():null
        ));
    }
}
