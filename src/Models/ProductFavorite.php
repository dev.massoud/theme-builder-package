<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Builder;

class ProductFavorite extends Model
{
    public $id;
    public ?Product $product;
    public $created_at;
    public $updated_at;
}
