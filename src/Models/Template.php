<?php
namespace WBuilder\Core\Models;


use WBuilder\Core\Types\ListOfMeta;

class Template extends Model
{
    public $id;
    public $key;
    public $type;
    public $name;
    public $image;
    public $title;
    public $config;
    public $status;
    public $author;
    /** @var ListOfMeta $meta */
    public ?ListOfMeta $meta;
    public ?Language $language;
    public ?Currency $currency;

    public function init($data){

    }

}
