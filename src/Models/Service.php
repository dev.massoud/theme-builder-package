<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Types\ListOfServiceRates;
class Service extends Model
{
    public $id;
    public $label;
    public $image;
    public ?ListOfServiceRates $rates;
    public ?Provider $provider;
    public $additional_data;
    public $created_at;
    public $sort;
    public $updated_at;

    public function init($data){
        $this->image = $data['image_url'];
    }
}
