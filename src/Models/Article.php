<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Types\ListOfArticleMedia;

class Article extends Model
{
    public $id;
    public $title;
    public $short_content;
    public $long_content;
    public $seo_keywords;
    public $additional_data;
    public $created_at;
    public $updated_at;
    public $image;
    public ?ListOfArticleMedia $media;

    public function init($data){
        $this->title = builder_translate($data['translations'], 'title');
        $this->short_content = builder_translate($data['translations'], 'short_content');
        $this->long_content = builder_translate($data['translations'], 'long_content');
        $this->seo_keywords = builder_translate($data['translations'], 'seo_keywords');
        $this->image = ($this->media->count() > 0)?$this->media->first()->url:null;
    }

    /**
     * return article title as slug split by dash for url
     * @return string
     */
    public function slug(){
        return \Illuminate\Support\Str::slug($this->title, "-");
    }
}
