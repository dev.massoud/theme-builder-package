<?php
namespace WBuilder\Core\Models;

class ProductColor extends Model
{
    public $id;
    public $product_id;
    public $color;
    public $title;
    public $stock;
    public $order;
    public $created_at;
    public $updated_at;

    public function init($data){
        if(isset($data['translations'])){
            $this->title = builder_translate($data['translations'], 'title');
        }
    }

}
