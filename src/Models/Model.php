<?php
namespace WBuilder\Core\Models;

use WBuilder\Core\Common\ParametersTrait;

abstract class Model
{
    public function __construct($data)
    {
        
        $reflect = new \ReflectionClass($this);
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($props as $prop) {
            $propName = $prop->getName();
            $rp = new \ReflectionProperty($this, $propName);
            if (isset($data[$propName])) {
                if($rp->hasType()){
                    $model = $rp->getType()->getName();
                    if(is_array($data[$propName]) && count($data[$propName]) > 0 && strpos($model, 'Types\ListOf')){
                        $list = new $model();
                        $object = $list->getClass();
                        foreach ($data[$propName] as $d){
                            if(is_array($d))
                                $list->insert(new $object($d));
                            else
                                $list->insert($d);
                        }
                        $this->$propName = $list;
                    }else{

                        switch ($model){
                            case "float":
                                $this->$propName = (float) $data[$propName];
                            break;
                            case "int":
                                $this->$propName = (int) $data[$propName];
                            break;
                            case "double":
                                $this->$propName = (double) $data[$propName];
                            break;
                        }
                        if(strpos($model, '\\')){
                            $this->$propName = new $model($data[$propName]);
                        }

                    }

                }else{
                    $this->$propName = $data[$propName];
                }

            }else{
                $this->$propName = null;
            }
        }
        if(method_exists($this, 'init'))
            $this->init($data);
    }
    function toJson() {
        return json_encode($this);
    }
}
