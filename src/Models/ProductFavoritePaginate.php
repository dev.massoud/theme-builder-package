<?php
namespace WBuilder\Core\Models;
use WBuilder\Core\Types\ListOfProductFavorite;

class ProductFavoritePaginate extends Model
{
    public $current_page;
    public $from;
    public $last_page;
    public $per_page;
    public $to;
    public $total;
    public ?ListOfProductFavorite $data;

}
