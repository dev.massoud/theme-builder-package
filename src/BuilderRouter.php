<?php


namespace WBuilder\Core;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Facade;
use Laravel\Ui\UiServiceProvider;
use WBuilder\Core\Common\Messages\RequestInterface;
use WBuilder\Core\Common\Messages\ResponseInterface;
use WBuilder\Core\Enums\Mode;
use WBuilder\Core\Providers\BuilderServiceProvider;

class BuilderRouter extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Builder Router';
    }

    /**
     * Register the typical authentication routes for an application.
     *
     * @param  array  $options
     * @return void
     *
     * @throws \RuntimeException
     */
    public static function routes(array $options = [])
    {
        if (! static::$app->providerIsLoaded(BuilderServiceProvider::class)) {
            throw new RuntimeException('In order to use the Auth::routes() method, please install the laravel/ui package.');
        }

        static::$app->make('router')->load($options);
    }

}
