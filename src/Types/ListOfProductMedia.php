<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\ProductMedia;

class ListOfProductMedia
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return ProductMedia[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param ProductMedia $data
     */
    public function insert(ProductMedia $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return ProductMedia
     */
    public function first() : ?ProductMedia{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return ProductMedia
     */
    public function last() : ?ProductMedia{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return ProductMedia
     */
    public function get($index) : ?ProductMedia{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return ProductMedia
     */
    public function search($value, $key = 'id') : ?ProductMedia{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return ProductMedia::class;
    }
    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
