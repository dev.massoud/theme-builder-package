<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\Category;

class ListOfCategories
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Category[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Category $data
     */
    public function insert(Category $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Category
     */
    public function first() : ?Category{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Category
     */
    public function last() : ?Category{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Category
     */
    public function get($index) : ?Category{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Category
     */
    public function search($value, $key = 'id') : ?Category{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }

    public function has($values, $key = 'id'){
        $collect = collect($this->items);
        return $collect->whereIn($key, $values);
    }

    public function getClass(){
        return Category::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
