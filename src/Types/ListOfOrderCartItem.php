<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\OrderCartItem;

class ListOfOrderCartItem
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return OrderCartItem[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param OrderCartItem $data
     */
    public function insert(OrderCartItem $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return OrderCartItem
     */
    public function first() : ?OrderCartItem{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return OrderCartItem
     */
    public function last() : ?OrderCartItem{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return OrderCartItem
     */
    public function get($index) : ?OrderCartItem{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return OrderCartItem
     */
    public function search($value, $key = 'id') : ?OrderCartItem{
        return $this->collect()->firstWhere($key, '=', $value);
    }

    public function getClass(){
        return OrderCartItem::class;
    }

    public function toJson(){
        $output = [];
        foreach ($this->items as $item){
            $output[] = $item->toJson();
        }
        return json_encode($output);
    }
}
