<?php
namespace WBuilder\Core\Types;

use Illuminate\Support\Collection;
use WBuilder\Core\Models\Product;

class ListOfProduct
{
    protected ?array $items = [];

    /**
     * return items as collection for search
     * @return Collection
     */
    public function collect(){
        return collect($this->items);
    }

    /**
     * return all items
     * @return Product[]
     */
    public function all(){
        return $this->items;
    }

    /**
     * return count of items
     * @return int
     */
    public function count(){
        return count($this->items);
    }

    /**
     * insert new item to items
     * @param Product $data
     */
    public function insert(Product $data){
        $this->items[] = $data;
    }

    /**
     * return first of items
     * @return Product
     */
    public function first() : ?Product{
        if(count($this->items) > 0)
            return $this->items[0];
        return null;
    }

    /**
     * return last of items
     * @return Product
     */
    public function last() : ?Product{
        if(count($this->items) > 0)
            return $this->items[count($this->items) - 1];
        return null;
    }

    /**
     * return get item by index
     * @param $index
     * @return Product
     */
    public function get($index) : ?Product{
        if(count($this->items) > 0 && isset($this->items[$index]))
            return $this->items[$index];
        return null;
    }

    /**
     * search in items
     * @param $value
     * @param $key
     * @return Product
     */
    public function search($value, $key = 'id') : ?Product{
        $collect = collect($this->items);
        return $collect->firstWhere($key, '=', $value);
    }


    

    public function getClass(){
        return Product::class;
    }
}
