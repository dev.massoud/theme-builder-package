<?php
namespace WBuilder\Core\Types;

class Price
{
    public float $sub_total;
    public float $discount;
    public float $tax;
    public float $total_price;

    /**
     * Price constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        $this->sub_total = $data['sub_total'];
        $this->discount = $data['discount'];
        $this->tax = $data['tax'];
        $this->total_price = $data['total_price'];
    }


}
