<?php
namespace WBuilder\Core\Common\Messages;

/**
 * Response Interface
 *
 * This interface class defines the standard functions that any Omnipay response
 * interface needs to be able to provide.  It is an extension of MessageInterface.
 *
 */
interface ResponseInterface extends MessageInterface
{
    /**
     * Get the original request which generated this response
     *
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * Is the response successful?
     *
     * @return boolean
     */
    public function isSuccessful();

    /**
     * Get the response data.
     *
     * @return mixed
     */
    public function getData();

    /**
     * Get the error.
     *
     * @return mixed
     */
    public function getError();

    /**
     * Get the error description.
     *
     * @return mixed
     */
    public function getErrorDescription();
}
