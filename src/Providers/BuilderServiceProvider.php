<?php


namespace WBuilder\Core\Providers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use WBuilder\Core\Builder;
use WBuilder\Core\Classes\WBUserProvider;
use WBuilder\Core\Commands\BuilderCommand;
use WBuilder\Core\Enums\Mode;
use WBuilder\Core\RouteMethods;

class BuilderServiceProvider extends ServiceProvider
{
    public function boot(Builder $builderLoader)
    {
        $this->offerPublishing();
        $this->registerCommands();

        Route::mixin(new RouteMethods);

        $this->app->singleton(Builder::class, function ($app) use ($builderLoader) {
            return $builderLoader;
        });

        Auth::provider('eloquent', function ($app, array $config) {
            return new WBUserProvider();
        });
    }
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/website-builder.php',
            'website-builder'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                BuilderCommand::class
            ]);
        }
    }
    protected function registerCommands()
    {

    }
    protected function offerPublishing()
    {
        if (! function_exists('config_path')) return;
        if(!file_exists(config_path('website-builder.php'))){
            copy(__DIR__.'/../../config/website-builder.php', config_path('website-builder.php'));
            $this->publishes([__DIR__.'/../../config/website-builder.php' => config_path('website-builder.php')], 'config');
        }
        $this->app->register(
            ThemeServiceProvider::class
        );
//        app()->basePath()."/app/Providers/"


    }
}
