<?php


namespace WBuilder\Core\Providers;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Shipu\Themevel\Facades\Theme;
use WBuilder\Core\Builder;
use WBuilder\Core\Enums\Mode;

class ThemeServiceProvider extends ServiceProvider
{
    public function boot(Builder $builder)
    {
        $template = $builder->template();
//        if(!Theme::has(strtolower($template->name))) abort(404);
//        $theme = Theme::get(strtolower($template->name));
//        Theme::set($theme['name']);

//        View::share('theme', $theme);
        View::share('template', $template);
    }
    public function register()
    {

    }
}
