<section class="pro-content empty-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pro-empty-page">
                    <h2 style="font-size: 150px;"><i class="far fa-check-circle"></i></h2>
                    <h1 >Thank You</h1>
                    <p>
                        You have successfully place your order. Your order id is 354365G4.
                        Go to the
                        <a href="dashboard.html#pills-billing.show.active" class="btn-link"><b>Order page</b></a>.
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>
