<h3>{{__('content.billing.address')}}</h3>
<form method="POST" action="{{route('checkout', route_params(['step' => 'billing-address']))}}">
    @csrf

    <div class="form-row">
        <div class="from-group col-md-12 mb-3">
            <div class="input-group select-control">
                <select class="form-control @error("billing_address_id") is-invalid @enderror" id="billing_address_id" name="billing_address_id">
                    <option value="-1" selected>{{__('content.add.new.address')}}</option>
                    @foreach(\Illuminate\Support\Facades\Auth::user()->billing_addresses->all() as $address)
                        <option value="{{$address->id}}" @if((old('billing_address_id', (@$address_info)?($address_info->billing_address_id):("")) == $address->id)) selected @endif>{{$address->title}}</option>
                    @endforeach
                </select>
                @error("billing_address_id")
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-6 mb-3">
            <div class="input-group ">
                <input type="text" class="form-control @error("title") is-invalid @enderror" id="title" name="title"
                       placeholder="{{__('content.title_text')}}"
                       value="{{(old("title", (@$address_info)?($address_info->title):("")))}}"
                >
                @error("title")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-6 mb-3">
            <div class="input-group"  >
                <input type="text" class="form-control @error("email") is-invalid @enderror" id="email" name="email"
                       placeholder="{{__('content.email')}}"
                       value="{{(old('email', ((@$address_info)?($address_info->email):(""))))}}"
                >
                @error("email")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-6 mb-3">
            <div class="input-group ">
                <input type="text" class="form-control @error("firstname") is-invalid @enderror" id="firstname"
                       name="firstname"
                       placeholder="{{__('content.firstname')}}"
                       value="{{(old('firstname', ((@$address_info)?($address_info->firstname):(""))))}}"
                >
                @error("firstname")
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-6 mb-3">
            <div class="input-group ">
                <input type="text" class="form-control @error("lastname") is-invalid @enderror" id="lastname"
                       name="lastname"
                       placeholder="{{__('content.lastname')}}"
                       value="{{(old('lastname', ((@$address_info)?($address_info->lastname):(""))))}}"
                >
                @error("lastname")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-12 mb-3">
            <div class="input-group ">
                <input type="text" class="form-control @error("address") is-invalid @enderror" id="address" name="address"
                       placeholder="{{__('content.address')}}"
                       value="{{(old('address', ((@$address_info)?($address_info->address):(""))))}}"
                >
                @error("address")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-4 mb-3">
            <div class="input-group select-control">
                <select class="form-control dd_countries @error("country_id") is-invalid @enderror" id="country_id" name="country_id">
                    <option value="" selected>{{__('content.select.country')}}</option>
                    @foreach($countries as $country)
                        <option value="{{$country->id}}" @if((old('country_id', ((@$address_info)?($address_info->country_id):(""))) == $country->id)) selected @endif>{{$country->name}}</option>
                    @endforeach
                </select>
                @error("country_id")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-4 mb-3">
            <div class="input-group select-control">
                <select class="form-control dd_states @error("state_id") is-invalid @enderror" id="state_id" name="state_id">
                    <option value="" selected>{{__('content.select.state')}}</option>
                    @if($states = (session('states'))?session('states'):$states)
                        @foreach($states as $state)
                            <option value="{{$state->id}}" @if((old('state_id', ((@$address_info)?($address_info->state_id):(""))) == $state->id)) selected @endif>{{$state->name}}</option>
                        @endforeach
                    @endif
                </select>
                @error("state_id")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-4 mb-3">
            <div class="input-group select-control">
                <select class="form-control dd_cities @error("city_id") is-invalid @enderror" id="city_id" name="city_id">
                    <option value="" selected>{{__('content.select.city')}}</option>
                    @if($cities = (session('cities'))?session('cities'):$cities)
                        @foreach($cities as $city)
                            <option value="{{$city->id}}" @if((old('city_id', ((@$address_info)?($address_info->city_id):(""))) == $city->id)) selected @endif>{{$city->name}}</option>
                        @endforeach
                    @endif
                </select>
                @error("city_id")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="billing_address_form from-group col-md-6 mb-3">
            <div class="input-group"  >
                <input type="text" class="form-control @error("postal_code") is-invalid @enderror" id="postal_code"
                       name="postal_code"
                       placeholder="{{__('content.postal.code')}}"
                       value="{{(old('postal_code', ((@$address_info)?($address_info->postal_code):(""))))}}"
                >
                @error("postal_code")
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

    </div>
    <div class="col-12 col-sm-12 justify-content-end btn-cont">
        <div class="row">
            <a href="{{route('checkout', route_params(['step' => 'shipping-address']))}}" class="btn btn-light">{{__('content.back')}}</a>
            <button type="submit" class="btn btn-secondary">{{__('content.continue')}}</button>
        </div>
    </div>
</form>
@push("scripts")
    <script>
        (function (window, document, $) {
            $('#billing_address_id').on('change', function (e){
                if(e.target.value.toString() === "-1"){
                    $('.billing_address_form').show();
                }else{
                    $('.billing_address_form').hide();
                }
            });
            @if(session('error'))
                $('.billing_address_form').show();
            @else
            @if(@$address_info && $address_info->billing_address_id  != "-1")
                $('.billing_address_form').hide();
            @endif
            @endif
        }(window, document, jQuery));
    </script>
@endpush
