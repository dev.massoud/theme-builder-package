@extends("checkout")

@section("checkout_content")
    @if(@$step == "shipping-address" || !@$step)
        @include("builder/checkout.shipping-address")
    @elseif(@$step == "billing-address")
        @include("builder/checkout.billing-address")
    @elseif(@$step == "shipping-method")
        @include("builder/checkout.shipping-methods")
    @elseif(@$step == "payment")
        @include("builder/checkout.payment")
    @endif
@endsection

@push("checkout_header")
    @include("builder/layouts/page-header", [
        "title" => "Checkout",
        "sub_title" => "Checkout your cart",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Checkout")
        )
    ])
@endpush
