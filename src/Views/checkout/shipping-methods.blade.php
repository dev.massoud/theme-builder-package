<h3>{{__('content.shipping.method')}}</h3>
<p>{{__('content.please.select.a.prefered.shipping.method')}}</p>
<form method="POST" action="{{route('checkout', route_params(['step' => 'shipping-method']))}}">
@csrf
<div class="row">
    <div class="col-12 col-sm-12 ">
        <ul class="payement-way @error("shipping_method") is-invalid @enderror">
            @if($shipping_services = builder()->deliveries())
                @foreach($shipping_services->all() as $index => $service)
                    <li>
                        <!-- Default checked -->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input radio_provider" value="{{$service->id}}" {{
                                    (old('shipping_method'))?
                                    ((old('shipping_method') == $service->id)?"checked":""):
                                    ((@$address_info)?(($address_info->shipping_method == $service->id)?"checked":""):(($index == 0)?"checked":""))
                                    }}
                                   id="defaultChecked_{{$index}}" name="shipping_method" >
                            <label class="custom-control-label"
                                   for="defaultChecked_{{$index}}">
                                {{$service->label}}<br/>
                                <img src="{{ ($service->image) }}" alt="{{$service->label}}" width="32"/>
                            </label>
                        </div>
                    </li>
                @endforeach
            @endif

        </ul>
        @error("shipping_method")
        <span class="invalid-feedback mb-3" >
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    @if($shipping_services)
    <div class="col-12 col-sm-6 mb-4">
        <h3>{{__('content.shipping.fee')}}</h3>
        @foreach($shipping_services->all() as $index => $service)
        <div @if($index > 0) style="display: none" @endif class="feebox @error("shipping_fee") is-invalid @enderror" id="fee_{{$service->id}}">
            @foreach($service->rates->all() as $i => $rate)
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" value="{{$rate->id}}" {{
                        (old('shipping_fee'))?
                        ((old('shipping_fee') == $rate->id)?"checked":""):
                        ((@$address_info)?(($address_info->shipping_fee == $rate->id)?"checked":""):(($index == 0 && $i == 0)?"checked":""))
                        }} id="feeChecked_{{$service->id}}_{{$rate->id}}" name="shipping_fee" >
                    <label class="custom-control-label"
                           for="feeChecked_{{$service->id}}_{{$rate->id}}">
                        {{$rate->title}} - @if($rate->price == 0) {{__('content.free')}} @else {{number_format($rate->price, 2)}}{{$rate->currency->symbol}} @endif
                    </label>
                </div>
            @endforeach

        </div>
        @endforeach
        @error("shipping_fee")
        <span class="invalid-feedback mb-3" >
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    @endif
</div>
<div class="row ">
    <div class="col-12 col-lg-12 justify-content-end btn-cont">
        <a href="{{route('checkout', route_params(['step' => 'billing-address']))}}" class="btn btn-light">{{__('content.back')}}</a>

        <button type="submit" class="btn btn-secondary">{{__('content.continue')}}</button>
    </div>

</div>
</form>
@push("scripts")
    <script>
        (function (window, document, $) {
            $('.radio_provider').on('change', function (e){
                $('.feebox').hide();
                $('#fee_'+e.target.value).show();
                if($('#fee_'+e.target.value).find('.custom-control-input').length > 0){
                    $($('#fee_'+e.target.value).find('.custom-control-input')[0]).prop('checked', true)
                }
            });
            @if(@$address_info)
                $('.feebox').hide();
                $('#fee_{{$address_info->shipping_method}}').show();
                $('#feeChecked_{{$address_info->shipping_method}}_{{$address_info->shipping_fee}}').prop('checked', true)
            @elseif(old('shipping_method'))
                $('.feebox').hide();
                $('#fee_{{old('shipping_method')}}').show();
            @endif

        }(window, document, jQuery));
    </script>
@endpush
