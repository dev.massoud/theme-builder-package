<table class="table top-table">
    <tbody>
    @foreach(builder()->cart()->get()->all() as $cart_item)
        <tr class="d-flex">
            <td class="col-12 col-md-2">
                <img class="img-fluid" src="{{ $cart_item->product->image }}" alt="{{$cart_item->product->title}}">
            </td>
            <td class="col-12 col-md-4">
                <div class="item-detail">
                    <span class="pro-category">{{$cart_item->product->categories()}}</span>
                    <h3>{{$cart_item->product->title}}</h3>
                    <div class="item-attributes"></div>

                </div>
            </td>
            <td class="col-12 col-md-1 item-price">
                @if($cart_item->product->price->summary->discount > 0)
                    <ins>{{number_format($cart_item->product->price->summary->total_price, 2)}}{{$cart_item->product->price->currency->symbol}}
                        <del>
                            {{number_format($cart_item->product->price->summary->sub_total, 2)}}{{$cart_item->product->price->currency->symbol}}
                        </del>
                    </ins>
                @else
                    <ins>{{number_format($cart_item->product->price->summary->total_price, 2)}}{{$cart_item->product->price->currency->symbol}}</ins>
                @endif
            </td>
            <td class="col-12 col-md-3 justify-content-center" >
                x {{$cart_item->quantity}}
            </td>
            <td class="col-12 col-md-2 item-total">{{number_format($cart_item->product->price->summary->total_price * $cart_item->quantity)}}{{builder()->getDefaultCurrency()->symbol}}</td>


        </tr>
    @endforeach
    </tbody>
</table>
<div class="row justify-content-end">
    <div class="col-12">
        <div class="bill-total">
            <ul>
                <li>{{__('content.subtotal')}}</li>
                <li>{{__('content.shipping')}}</li>
                <li>{{__('content.coupon')}}</li>
                <li class="text-danger">{{__('content.total')}}</li>
            </ul>
            <ul>
                <li>{{number_format(builder()->cart()->summary()->getSubTotal(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</li>
                <li>{{number_format(builder()->cart()->summary()->getShipping(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</li>
                <li>{{number_format(builder()->cart()->summary()->getDiscount(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</li>
                <li class="text-danger">{{number_format(builder()->cart()->summary()->getTotal(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</li>
            </ul>

            <input type="hidden" name="currency_id" value="{{builder()->getDefaultCurrency()->id}}">
            <input type="hidden" name="subtotal" value="{{builder()->cart()->summary()->getSubTotal()}}">
            <input type="hidden" name="shipping" value="{{builder()->cart()->summary()->getShipping()}}">
            <input type="hidden" name="coupon" value="{{builder()->cart()->summary()->getDiscount()}}">
            <input type="hidden" name="total" value="{{builder()->cart()->summary()->getTotal()}}">
        </div>
    </div>
</div>
