<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
<div class="row ">
    <div class="col-12 col-md-6">
        <div class="row ">
            <div id="quickViewCarousel" class="carousel slide" data-ride="carousel">
                <!-- The slideshow -->
                <div class="carousel-inner">
                    @if($product->media->count() > 0)
                        @foreach($product->media->all() as $index => $media)
                            <div class="carousel-item @if($index == 0) active @endif">
                                <img class="img-fluid" src="{{ $media->url }}" />
                            </div>
                        @endforeach
                    @endif

                </div>

                <a class="carousel-control-prev" href="#quickViewCarousel" data-slide="prev">
                    <span class="fas fa-angle-left "></span>
                </a>
                <a class="carousel-control-next" href="#quickViewCarousel" data-slide="next">
                    <span class="fas fa-angle-right "></span>
                </a>

            </div>
        </div>
    </div>
    <div class="col-12 col-md-6 pro-description">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="badges">
                    @if($categoryName = $product->has_category("featured"))
                        <div class="badge badge-info">
                            {{$categoryName}}
                        </div>
                    @endif
                    @if($product->stock <= 0)
                        <div class="badge badge-dark">
                            {{__('content.out.of.stock')}}
                        </div>
                    @endif
                </div>
                <h3 >{{$product->title}}</h3>

                <div class="pro-price">
                    @if($product->price->summary->discount > 0)
                        <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}
                            <del>
                                {{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}
                            </del>
                        </ins>
                    @else
                        <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}</ins>
                    @endif
                </div>
                <div class="pro-rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                    <a href="#review" class="btn-link">2 reviews</a>
                </div>
                <div class="pro-info">
                    <div class="pro-single-info"><b>{{__('content.product.id')}} :</b><span>{{$product->id}}</span></div>
                    <div class="pro-single-info"><b>{{__('content.category')}} :</b><span><a href="#">{{$product->categories()}}</a></span></div>
                    @if($seo_keywords = $product->seo_keywords)
                    <div class="pro-single-info">
                        <b>{{__('content.tags')}} :</b>
                        <ul>
                            @foreach(explode(",", $seo_keywords) as $keyword)
                                <li><a href="#">{{$keyword}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="pro-single-info"><b>{{__('content.available')}} :</b><span class="text-secondary">@if($product->stock <= 0) {{__('content.out.of.stock')}} @else {{__('content.in.stock')}} @endif</span></div>
                </div>

                <div class="pro-options">
                    @if($product->colors->count() > 0)
                    <div class="color-option">
                        <b>{{__('content.color')}} : </b>
                        <ul class="product-model">
                            @foreach($product->colors->all() as $color_item)
                                <li class="@if(@$color && $color->id == $color_item->id) active @endif"><a style="background-color: {{$color_item->color}}" href="javascript:void(0);" data-cid="{{$color_item->id}}" data-pid="{{$product->id}}"></a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if($product->sizes->count() > 0)
                    <div class="size-option">
                        <b>{{__('content.size')}} : </b>
                        <ul class="product-model1">
                            @foreach($product->sizes->all() as $size)
                                <li><a class="size-select" href="javascript:void(0);">{{$size->size}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>

                <div class="pro-quantiy">
                    <div class="input-group-control">
                        <input type="text" id="quantity1" name="quantity" class="form-control" maxlength="2" value="1" size="2">
                        <span class="input-group-btn">
                            <button type="button" value="quantity1" class="quantity-plus1 btn btn-outline-secondary" data-type="plus" data-field="">
                                <i class="fas fa-plus"></i>
                            </button>
                            <button type="button" value="quantity1" class="quantity-minus1 btn btn-outline-secondary" data-type="minus" data-field="">
                                <i class="fas fa-minus"></i>
                            </button>
                        </span>
                    </div>
                    <button type="button" class="btn btn-secondary btn-lg" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">{{__('content.add.to.cart')}}</button>
                    <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                        @csrf
                        <input type="hidden" name="pid" value="{{$product->id}}" />
                        <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                        <input type="hidden" name="quantity" value="1" />
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
