<div class="row">
    <div class="col-12 col-lg-3">
        @include("builder/shop/filter")
    </div>
    <div class="col-12 col-lg-9">
        @include("builder/shop/toolbar")
        <div id="swap" class="productbar">
            @include("builder/shop/list")
        </div>
        @include("builder/shop/pagination")
    </div>
</div>
