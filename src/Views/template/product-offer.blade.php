<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
<div class="">
    <div class="row align-items-center">
        <div class="col-12 col-lg-6">
            <div class="pro-thumb ">
                <a href="{{route('product', route_params(['pid' => $product->id, 'title' => $product->slug()]))}}">
                    <img class="img-fluid" src="{{ $product->image }}" alt="{{$product->title}}">
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-6">
            <div class="pro-info">
                <h2>{{__('content.special.offers')}}</h2>
                <h3><a href="{{route('product', route_params(['pid' => $product->id, 'title' => $product->slug()]))}}">{{$product->title}}</a></h3>
                <p>{!! nl2br($product->short_content) !!}</p>
                <div class="pro-price">
                    @if($product->price->summary->discount > 0)
                        <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}</ins>
                        <del>{{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}</del>
                    @else
                        <ins>{{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}</ins>
                    @endif
                </div>
                <button type="button" class="btn btn-secondary" onclick="document.getElementById('add-to-cart-offer-form-{{$product->id}}').submit()">{{__('content.add.to.cart')}}</button>
                <form id="add-to-cart-offer-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                    @csrf
                    <input type="hidden" name="pid" value="{{$product->id}}" />
                    <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                    <input type="hidden" name="quantity" value="1" />
                </form>

            </div>
        </div>
    </div>
</div>
