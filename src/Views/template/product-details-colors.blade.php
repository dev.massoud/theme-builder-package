<?php /** @var \WBuilder\Core\Types\ListOfProductColor $colors **/ ?>
<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
@if($colors->count() > 0)
    <div class="color-option">
        <b>{{__('content.color')}} : </b>
        <ul class="product-model">
            @foreach($colors->all() as $color_item)
                <li class="@if($color && $color->id == $color_item->id) active @endif"><a style="background-color: {{$color_item->color}}" href="{{ $product->url($color_item->id) }}" data-cid="{{$color_item->id}}" data-pid="{{$product->id}}"></a></li>
            @endforeach
        </ul>
    </div>
@endif
