<?php /** @var \WBuilder\Core\Types\ListOfProductSize $sizes */ ?>
@if($sizes->count() > 0)
    <div class="size-option">
        <ul class="product4">
            @foreach($sizes->all() as $size)
                <li><a class="size-select" href="javascript:void(0);">{{$size->size}}</a></li>
            @endforeach
        </ul>
    </div>
@endif
