<?php /** @var \WBuilder\Core\Types\ListOfProductColor $colors */ ?>
@if($colors->count() > 0)
    <div class="color-option">
        <ul class="product1">
            @foreach($colors->all() as $color)
                <li><a style="background-color: {{$color->color}}" href="javascript:void(0);" data-cid="{{$color->id}}" data-pid="{{$product->id}}"></a></li>
            @endforeach
        </ul>
    </div>
@endif
