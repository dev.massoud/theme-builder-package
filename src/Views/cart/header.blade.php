@if(!@$cart)
    @php($cart = builder()->cart()->get())
@endif
<ul class="shopping-cart-items">
<?php /**  @var \WBuilder\Core\Classes\Cart\CartItem $item **/ ?>
@foreach($cart->all() as $item)
    <li>
        <div class="item-thumb">
            <div class="image">
                <img class="img-fluid" src="{{ $item->product->image }}" alt="{{$item->product->title}}">
            </div>
        </div>
        <div class="item-detail">
            <h3>{{$item->product->title}}</h3>
            <div class="item-s">{{$item->quantity}} x {{$item->product->price->summary->total_price}}{{builder()->getDefaultCurrency()->symbol}} <i class="fas fa-trash" onclick="document.getElementById('cart-item-delete-form-{{$item->id}}').submit()"></i></div>
        </div>
        <form id="cart-item-delete-form-{{$item->id}}" method="post" action="{{ route('delete-cart-item', route_params()) }}">
            @csrf
            <input type="hidden" name="id" value="{{$item->id}}" />
        </form>
    </li>
@endforeach

<li>
    <span class="item-summary">{{ __('content.total') }}&nbsp;:&nbsp;<span>{{number_format(builder()->cart()->summary()->getTotal(), 2)}}{{builder()->getDefaultCurrency()->symbol}}</span></span>
</li>
<li>
    <a class="btn btn-link btn-block " href="{{route('cart', route_params())}}">{{ __('content.view.cart') }}</a>
    <a class="btn btn-secondary btn-block  " href="{{route('checkout', route_params())}}">{{ __('content.checkout') }}</a>
</li>
</ul>
