<?php /** @var \WBuilder\Core\Models\ProductFavorite $favorite */ ?>
<tr class="d-flex">
    <td class="col-12 col-lg-2">
        <img class="img-fluid" src="{{ $favorite->product->image }}" alt="{{$favorite->product->title}}">
    </td>
    <td class="col-12 col-lg-3">
        <div class="item-detail">
            <span class="pro-category">{{$favorite->product->categories()}}</span>
            <h3>{{$favorite->product->title}}</h3>
            <div class="item-attributes"></div>

        </div>
    </td>
    <td class="col-12 col-lg-2 item-price">
        <div class="pro-price">
            @if($favorite->product->price->summary->discount > 0)
                <ins>{{number_format($favorite->product->price->summary->total_price, 2)}} {{$favorite->product->price->currency->symbol}}
                    <del>
                        {{number_format($favorite->product->price->summary->sub_total, 2)}} {{$favorite->product->price->currency->symbol}}
                    </del>
                </ins>
            @else
                <ins>{{number_format($favorite->product->price->summary->total_price, 2)}} {{$favorite->product->price->currency->symbol}}</ins>
            @endif
        </div>
    </td>
    @if($product->stock > 0)
        <td class="col-12 col-lg-2 text-success">{{__('content.in.stock')}}</td>
    @else
        <td class="col-12 col-lg-2 text-danger">{{__('content.out.of.stock')}}</td>
    @endif


    <td class="col-12 col-lg-3 align-middle">
        @if($product->stock > 0)
            <button type="button" class="btn btn-secondary btn-lg" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">{{__('content.add.to.cart')}}</button>
            <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                @csrf
                <input type="hidden" name="pid" value="{{$product->id}}" />
                <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                <input type="hidden" name="quantity" value="1" />
            </form>
        @else
            <button type="button" class="btn btn-danger btn-lg " disabled>{{__('content.out.stock')}}</button>
        @endif

    </td>

</tr>
