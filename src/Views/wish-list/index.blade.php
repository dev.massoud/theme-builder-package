<section class="pro-content wishlist-content my-4">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 ">
                <table class="table top-table">
                <tbody>
                    @foreach($products->data->all() as $favorite)
                        @include("builder/wish-list/wish-item", ['product' => $favorite->product])
                    @endforeach

{{--                    @include("builder/wish-list/wish-item", ['image' => asset('assets/images/product_images/product_image_2.jpg'), 'title' => 'Modren Wood Chair', 'stock' => false])--}}
{{--                    @include("builder/wish-list/wish-item", ['image' => asset('assets/images/product_images/product_image_3.jpg'), 'title' => 'White Plastic Chair', 'stock' => true])--}}
                </tbody>
                </table>
                @include("builder/wish-list/pagination")

            </div>
        </div>
    </div>
</section>
