@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "title" => "Shop",
        "sub_title" => "Search in products",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Shop")
        )
    ])
    @include("builder/shop/index")
@endsection
@section("modals")
    @include("builder/layouts/quick-view-modal")
@endsection
