<?php $news_articles = builder()->articles('news_articles'); ?>
@if($news_articles->total > 0)
<section class="pro-blog-content pro-content">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <div class="pro-heading-title">
                    @if($news_title = builder()->key('news_title'))
                        <h2 class="ref_elm_news_title">{{$news_title}}</h2>
                    @endif
                    @if($news_sub_title = builder()->key('news_sub_title'))
                        <p class="ref_elm_news_sub_title">{{$news_sub_title}}</p>
                    @endif
                </div>
            </div>
            <div class="col-12 col-md-6"></div>
        </div>
        <div class="row">
            <div class="col-12 ref_elm_news_articles">
                <template>
                    <div class="ref_elm_post_@ID@">
                        <div class="blog">
                            <div class="blog-thumb">
                                <img class="img-fluid ref_elm_post_@ID@_image" src="@IMAGE@" alt="@TITLE@">
                            </div>
                            <div class="blog-info">
                                <div class="blog-meta">
                                    <a >
                                        <i class="far fa-calendar-alt"></i>
                                        {{date("j F, Y")}}
                                    </a>
                                </div>
                                <h3><a href="blog-page1.html" class="ref_elm_post_@ID@_title">@TITLE@</a></h3>
                                <p class="ref_elm_post_@ID@_short_content">@SHORT_CONTENT@</p>

                            </div>
                        </div>
                    </div>
                </template>
                <div class="blog-carousel-js row ref_elm_container">
                    @foreach($news_articles->data->all() as $news_item)
                        <div class="ref_elm_post_{{$news_item->id}}">
                            <div class="blog">
                                <div class="blog-thumb">
                                    <img class="img-fluid ref_elm_post_{{$news_item->id}}_image" src="{{ $news_item->image }}" alt="{{$news_item->title}}">
                                </div>
                                <div class="blog-info">
                                    <div class="blog-meta">
                                        <a >
                                            <i class="far fa-calendar-alt"></i>
                                            {{date("j F, Y", strtotime($news_item->created_at))}}
                                        </a>
                                    </div>
                                    <h3><a href="{{route('article', route_params(['id' => $news_item->id, 'title' => $news_item->slug()]))}}" class="ref_elm_post_{{$news_item->id}}_title">{{$news_item->title}}</a></h3>
                                    <p class="ref_elm_post_{{$news_item->id}}_short_content">{{$news_item->short_content}}</p>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
