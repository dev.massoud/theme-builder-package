<?php $clients = builder()->articles('clients'); ?>
@if($clients->total > 0)
<div class="brands-content pro-content" id="clients_section">
    <div class="container ref_elm_clients">
        <template>
            <div class="brand ref_elm_post_@ID@">
                <img class="ref_elm_post_@ID@_image" src="@IMAGE@" alt="brand-logo">
            </div>
        </template>
        <div class="brands-carousel-js row ref_elm_container">
            @foreach($clients->data->all() as $client_item)
                <div class="brand ref_elm_post_{{$client_item->id}}">
                    <img class="ref_elm_post_{{$client_item->id}}_image" src="{{ $client_item->image }}" alt="brand-logo" />
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif
