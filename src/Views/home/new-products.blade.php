<?php $new_products = builder()->productByGroup('new_products') ?>
@if($new_products->total > 0)
<section class="tabs-content pro-content ref_elm_new_products" >
    <div class="container">
        <div class="row ">
            <div class="col-12 col-md-12">
                <div class="pro-heading-title">
                    @if($featured_title = builder()->key('featured_title'))
                        <h2 class="ref_elm_featured_title">{{$featured_title}}</h2>
                    @endif
                    @if($featured_description = builder()->key('featured_description'))
                        <p class="ref_elm_featured_description">{{$featured_description}}</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ">
                <div class="tab-carousel-js row ref_elm_container" data-template="template/product">
                    @foreach($new_products->data->all() as $product)
                        @include("builder/template/product", ["product" => $product])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
