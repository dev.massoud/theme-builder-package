<?php $slideshow = builder()->articles('slideshow') ?>
@if($slideshow && $slideshow->total > 0)
    <!-- Revolution Layer Slider -->
    <div id="rev_slider_1077_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="scroll-effect136" data-source="gallery" style="padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_1077_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
            <ul>
                <!-- SLIDE  3-->
                @foreach($slideshow->data->all() as $index => $slideshow_item)
                    @if(key_exists("align_view", $slideshow_item->additional_data))
                        @include("builder/layouts/slideshow/".$slideshow_item->additional_data['align_view'], [
                            "id" => $slideshow_item->id,
                            "title" => $slideshow_item->title,
                            "short_content" => $slideshow_item->short_content,
                            "image" => $slideshow_item->image,
                            "index" => $index
                        ])
                    @endif
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div><!-- END REVOLUTION SLIDER -->
@endif
