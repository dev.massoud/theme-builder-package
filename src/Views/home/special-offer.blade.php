<div class="pro-banners-content pro-content">
    <div class="fullwidth-banner ref_elm_special_offers_background" style="background-image: url('{{ builder_image_url(builder()->key('special_offers_background', 'images/banners/furniture-bg.jpg')) }}');">
        <div class="col-12 col-lg-6 parallax-banner-text">
            @if($special_offers_title = builder()->key('special_offers_title'))
                <h3 class="sub-title ref_elm_special_offers_title">{{$special_offers_title}}</h3>
            @endif
            @if($special_offers_sub_title = builder()->key('special_offers_sub_title'))
                <h2 class="ref_elm_special_offers_sub_title">{{$special_offers_sub_title}}</h2>
            @endif
            @if($special_offers_description = builder()->key('special_offers_description'))
                <p class="ref_elm_special_offers_description">{!! nl2br($special_offers_description) !!}</p>
            @endif
            <button class="btn btn-secondary">{{__('content.discover.now')}}</button>
        </div>
    </div>
</div>
