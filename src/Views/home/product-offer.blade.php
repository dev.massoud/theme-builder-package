<?php $offers_products = builder()->productByGroup('product_offers'); ?>
@if($offers_products->total > 0)
<section class="pro-fs-content pro-content ref_elm_product_offers">
    <div class="container">
        <div class="flashsale-carousel-js row ref_elm_container" data-template="template/product-offer">
            @foreach($offers_products->data->all() as $product)
                @include("builder/template.product-offer", ["product" => $product])
            @endforeach
        </div>
    </div>
</section>
@endif
