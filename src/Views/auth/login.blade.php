@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "title" => "Login",
        "sub_title" => "Login for shopping",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Login")
        )
    ])
    <div class="page-area pro-content">
        <div class="container">


            <div class="row justify-content-center account-content">
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="col-12  px-0">
                        <div class="registration-process">
                            <form action="{{ route('login', route_params()) }}" method="POST">
                                @csrf
                                <div class="from-group mb-3">
                                    <div class="input-group col-12">
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="{{__('content.enter.your.email')}}" required>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="from-group mb-3">

                                    <div class="input-group col-12">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="{{__('content.enter.your.password')}}" required>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 Login-btn">
                                    <button type="submit" class="btn btn-secondary">{{__('content.login')}}</button>
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request', route_params()) }}" class="btn btn-link">{{__('content.forgot.password')}}</a>
                                    @endif

                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
            <div class="row justify-content-center text-center">
                <div class="registration-socials">
                    <div class="col-12 col-sm-12 left">
                        <div class="from-group">
                            <button type="button" class="btn btn-google"><i class="fab fa-google-plus-g"></i>&nbsp;{{__('content.login.with.google')}}</button>
                            <button type="button" class="btn btn-facebook"><i class="fab fa-facebook-f"></i>&nbsp;{{__('content.login.with.facebook')}}</button>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
@section("modals")
    @include("builder/layouts/quick-view-modal")
@endsection
