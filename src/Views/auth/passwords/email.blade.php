@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "title" => __('content.reset.password'),
        "sub_title" => __('content.you.can.reset.your.password'),
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => __('content.home')),
            array("url" => route('login', route_params()), "title" => __('content.login')),
            array("url" => "", "title" => __('content.reset.password'))
        )
    ])
    <div class="page-area pro-content">
        <div class="container">


            <div class="row justify-content-center account-content">
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="col-12  px-0">
                        <div class="registration-process">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form action="{{ route('password.email', route_params()) }}" method="POST">
                                @csrf
                                <div class="from-group mb-3">
                                    <div class="input-group col-12">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                               value="{{ old('email') }}" required autocomplete="email"
                                               autofocus
                                               placeholder="{{__('content.enter.your.email')}}"
                                        >

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 Login-btn">
                                    <button type="submit" class="btn btn-secondary">{{__('content.send.reset.password.link')}}</button>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section("modals")

@endsection
