<?php /** @var \WBuilder\Core\Models\Product $product **/ ?>
<div class="row">
    <div class="col-12 col-md-12">
        <div class="badges">
            @if($categoryName = $product->has_category("featured"))
                <div class="badge badge-info">
                    {{$categoryName}}
                </div>
            @endif
            @if($product->stock <= 0)
                <div class="badge badge-dark">
                    {{__('content.out.of.stock')}}
                </div>
            @endif
        </div>
        <h2 class="ref_elm_product_item_{{$product->id}}_title">{{$product->title}}</h2>

        <div class="pro-price ref_elm_price_{{$product->id}}_value">
            @if($product->price->summary->discount > 0)
                <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}
                    <del>
                        {{number_format($product->price->summary->sub_total, 2)}} {{$product->price->currency->symbol}}
                    </del>
                </ins>
            @else
                <ins>{{number_format($product->price->summary->total_price, 2)}} {{$product->price->currency->symbol}}</ins>
            @endif

        </div>
        <div class="pro-rating">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star-half-alt"></i>
            <a href="#review" class="btn-link">2 reviews</a>
        </div>
        <div class="pro-info">
            <div class="pro-single-info"><b>{{__('content.product.id')}} :</b><span>{{$product->id}}</span></div>
            <div class="pro-single-info"><b>{{__('content.category')}} :</b><span><a href="#">{{$product->categories()}}</a></span></div>
            @php($seo_keywords = $product->seo_keywords)
            @if($seo_keywords)
                <div class="pro-single-info">
                    <b>{{__('content.tags')}} :</b>
                    <ul>
                        @foreach(explode(",", $seo_keywords) as $keyword)
                            <li><a href="#">{{$keyword}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="pro-single-info"><b>{{__('content.available')}} :</b><span class="text-secondary">@if($product->stock <= 0) {{__('content.out.of.stock')}} @else {{__('content.in.stock')}} @endif</span></div>
        </div>

        <div class="pro-options">
            <div class="ref_elm_{{$product->id}}_product_colors" data-template="template/product-details-colors">
                @include("builder/template.product-details-colors", ["colors" => $product->colors])
            </div>
            <div class="ref_elm_{{$product->id}}_product_sizes" data-template="template/product-details-sizes">
                @include("builder/template.product-details-sizes", ["sizes" => $product->sizes])
            </div>
        </div>
        <div class="pro-quantiy">
            <div class="input-group-control">
                <input type="text" id="quantity1" name="quantity" class="form-control" maxlength="2" value="1" size="2">
                <span class="input-group-btn">
                            <button type="button" value="quantity1" class="quantity-plus1 btn btn-outline-secondary" data-type="plus" data-field="">
                                <i class="fas fa-plus"></i>
                            </button>
                            <button type="button" value="quantity1" class="quantity-minus1 btn btn-outline-secondary" data-type="minus" data-field="">
                                <i class="fas fa-minus"></i>
                            </button>
                        </span>
            </div>
            <button type="button" class="btn btn-secondary btn-lg" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">{{__('content.add.to.cart')}}</button>
            <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                @csrf
                <input type="hidden" name="pid" value="{{$product->id}}" />
                <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                <input type="hidden" name="quantity" value="1" />
            </form>
        </div>
        <div class="pro-buttons">
            @if($product->is_favorite())
                <button class="btn btn-link" onclick="document.getElementById('frm-product-to-favorite-{{$product->id}}').submit()"><i class="fas fa-heart"></i><span>{{__('content.delete.from.wishlist')}}</span></button>
            @else
                <button class="btn btn-link" onclick="document.getElementById('frm-product-to-favorite-{{$product->id}}').submit()"><i class="far fa-heart"></i><span>{{__('content.add.to.wishlist')}}</span></button>
            @endif

            <form action="{{route('add-to-favorite', route_params())}}" method="post" id="frm-product-to-favorite-{{$product->id}}">
                @csrf
                <input type="hidden" value="{{$product->id}}" name="product_id" />
            </form>
            <button class="btn btn-link" onclick="notificationCompare();"><i class="fas fa-align-right"></i>{{__('content.add.to.compare')}}</button>
        </div>
        <div class="pro-social-share">
            <!-- AddToAny BEGIN -->
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_email"></a>
            </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
        </div>
    </div>
</div>
