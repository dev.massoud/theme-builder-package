<div class="row">
    <div class="col-12 col-md-12 discrption-product">
        <div class="nav nav-pills" role="tablist">
            <a class="nav-link nav-item  active" href="#description" id="description-tab" data-toggle="pill" role="tab">Description</a>
            <a class="nav-link nav-item " href="#additionalInfo" id="additional-info-tab" data-toggle="pill" role="tab" >Additional information</a>
            <a class="nav-link nav-item" href="#review" id="review-tab" data-toggle="pill" role="tab" >Reviews</a>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active show" id="description" aria-labelledby="description-tab">
                @include("builder/product-detail/description")
            </div>
            <div role="tabpanel" class="tab-pane fade" id="additionalInfo" aria-labelledby="additional-info-tab">
                @include("builder/product-detail/additional-info")
            </div>
            <div role="tabpanel" class="tab-pane fade " id="review" aria-labelledby="review-tab">
                @include("builder/product-detail/reviews")

            </div>
        </div>
    </div>

</div>
