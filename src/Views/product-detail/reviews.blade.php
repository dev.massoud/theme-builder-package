<div class="reviews">
    <div class="review-bubbles">
        <h2>
            Customer Reviews
        </h2>
        <div class="review-bubble-single">
            <div class="review-bubble-bg">
                <div class="pro-rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>

                </div>
                <h4>Good</h4>
                <span>Sep 20, 2019</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
            </div>

        </div>
        <div class="review-bubble-single">
            <div class="review-bubble-bg">
                <div class="pro-rating">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>

                </div>
                <h4>NICE WORK!!!</h4>
                <span>Sep 20, 2019</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
            </div>

        </div>

    </div>
    <div class="write-review">
        <h2>Write a Review</h2>
        <div class="write-review-box">
            <div class="from-group row mb-3">
                <div class="col-12">
                    <label for="inlineFormInputGroup01">Name</label></div>
                <div class="input-group col-12">

                    <input type="text" class="form-control" id="inlineFormInputGroup01" placeholder="Enter Your Name">
                </div>
            </div>
            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup1">Email Address</label></div>
                <div class="input-group col-12">

                    <input type="text" class="form-control" id="inlineFormInputGroup1" placeholder="Enter Your Email">
                </div>
            </div>
            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup2">Rating</label></div>
                <div class="pro-rating  col-12">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>

                </div>
            </div>

            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup2">Review Title</label></div>
                <div class="input-group col-12">

                    <input type="text" class="form-control" id="inlineFormInputGroup2" placeholder="Title of Review">
                </div>
            </div>
            <div class="from-group row mb-3">
                <div class="col-12"> <label for="inlineFormInputGroup3">Review Body</label></div>
                <div class="input-group col-12">

                    <textarea class="form-control" id="inlineFormInputGroup3" placeholder="Write Your Review"></textarea>
                </div>
            </div>
            <div class="from-group">
                <button type="button" class="btn btn-secondary swipe-to-top">Submit</button>

            </div>
        </div>
    </div>
</div>
