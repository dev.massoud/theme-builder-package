@php($offers_products = $settings->products(['top-offers']))
@if(count($offers_products) > 0)
    <div class="col-12 col-lg-4  d-lg-block d-xl-block blog-menu">
        <div class="widget">
            <h3 class="widget-title">{{__('content.top.offers')}}</h3>
            @foreach($offers_products as $product)
                <div class="media">
                    <img src="{{ resource_url($settings->defaultImage($product)) }}" alt="{{$settings->translations($product, 'title')}}" width="100">
                    <div class="media-body">
                        <h3>
                            <a href="{{route('product', route_params(['pid' => $product->id, 'title' => \Illuminate\Support\Str::slug($settings->translations($product, 'title'), "-")]))}}">
                                {{$settings->translations($product, 'title')}}
                            </a>
                        </h3>
                        <div class="pro-price">
                            @if($settings->price($product, 'discount') > 0)
                                <ins>{{number_format($settings->price($product, 'price', false) - $settings->price($product, 'discount', false), 2)}}{{$settings->currency($product)}}</ins>
                                <del>{{$settings->price($product, 'price', true)}}</del>
                            @else
                                <ins>{{$settings->price($product, 'price', true)}}</ins>
                            @endif
                        </div>
                        <button type="button" class="btn btn-secondary" onclick="document.getElementById('add-to-cart-form-{{$product->id}}').submit()">{{__('content.add.to.cart')}}</button>
                        <form id="add-to-cart-form-{{$product->id}}" method="post" action="{{ route('add-to-cart', route_params()) }}">
                            @csrf
                            <input type="hidden" name="pid" value="{{$product->id}}" />
                            <input type="hidden" name="cid" value="{{(@$color?$color->id:"")}}" />
                            <input type="hidden" name="quantity" value="1" />
                        </form>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
