<div class="col-12 col-lg-8 pro-blog-content">
    <div class="row">
        @foreach($articles->items() as $article)
            <div class="col-12 col-md-6">
                <div class="blog">
                    <div class="blog-thumb">
                        <img class="img-fluid" src="{{resource_url($settings->defaultImage($article))}}" alt="{{$settings->translations($article, 'title')}}" />
                    </div>

                    <div class="blog-info">
                        <div class="blog-meta">
                            <a >
                                <i class="far fa-calendar-alt"></i>
                                {{date("j F, Y", strtotime($article->created_at))}}
                            </a>
                        </div>
                        <h3><a href="{{route('article', route_params(['id' => $article->id, 'title' => \Illuminate\Support\Str::slug($settings->translations($article, 'title'), "-")]))}}">{{$settings->translations($article, 'title')}}</a></h3>
                        <p>{{$settings->translations($article, 'short_content')}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {{ $articles->appends($parameters)->links('layouts.pagination', ['paginator' => $articles]) }}

</div>
