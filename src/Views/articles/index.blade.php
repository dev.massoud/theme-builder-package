<section class="pro-content blog-content">
    <div class="container">
        <div class="row">
            @include("builder/articles/list")
            @include("builder/articles/right-side")

        </div>
    </div>
</section>
