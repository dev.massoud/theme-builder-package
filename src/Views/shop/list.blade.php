<?php /** @var \WBuilder\Core\Models\ProductPaginate $products */ ?>
<div class="row">
    @php
        $col = 'col-12 col-md-4';
        if(request()->has('col')){
            if(request()->get('col') == 4)
                $col = 'col-12 col-md-3';
            else if(request()->get('col') == 3)
                $col = 'col-12 col-md-4';
            else if(request()->get('col') == 2)
                $col = 'col-12 col-md-6';
            else if(request()->get('col') == 1)
                $col = 'col-12';
        }
    @endphp
    @foreach($products->data->all() as $product)
        @include("builder/template/product", ["product" => $product, "class" => $col])
    @endforeach
</div>
