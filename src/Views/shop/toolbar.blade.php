<?php /** @var \WBuilder\Core\Models\ProductPaginate $products */ ?>
<div class="toolbar">
    <div class="toolbox">
        <div class="toolbox-left">
            <div class="toolbox-info">
                {{__('content.showing')}} <span>{{$products->from}} {{__('content.of')}} {{$products->total}}</span> {{__('content.products')}}
            </div><!-- End .toolbox-info -->
        </div><!-- End .toolbox-left -->
        <div class="toolbox-right">
            <div class="toolbox-sort">
                <label for="sortby">{{__('content.sort.by')}}:</label>
                <div class="select-control">
                    <select name="sort_by" id="sort_by" class="form-control" onclick="setQueryStringParameter('sort_by', this.value)">
                        <option value="created_at" @if(request()->get('sort_by') == "created_at") selected @endif>{{__('content.date')}}</option>
                        <option value="title" @if(request()->get('sort_by') == "title") selected @endif>{{__('content.title')}}</option>

                    </select>
                </div>
            </div><!-- End .toolbox-sort -->
            <div class="toolbox-layout">
                <a href="{{ route('shop', route_params(['col' => 4])) }}" class="btn-layout @if(request()->get('col') == 4) active @endif">
                    <svg width="22" height="10">
                        <rect x="0" y="0" width="4" height="4"></rect>
                        <rect x="6" y="0" width="4" height="4"></rect>
                        <rect x="12" y="0" width="4" height="4"></rect>
                        <rect x="18" y="0" width="4" height="4"></rect>
                        <rect x="0" y="6" width="4" height="4"></rect>
                        <rect x="6" y="6" width="4" height="4"></rect>
                        <rect x="12" y="6" width="4" height="4"></rect>
                        <rect x="18" y="6" width="4" height="4"></rect>
                    </svg>
                </a>
                <a href="{{ route('shop', route_params(['col' => 3])) }}" class="btn-layout @if(!request()->has('col') || request()->get('col') == 3) active @endif">
                    <svg width="16" height="10">
                        <rect x="0" y="0" width="4" height="4"></rect>
                        <rect x="6" y="0" width="4" height="4"></rect>
                        <rect x="12" y="0" width="4" height="4"></rect>
                        <rect x="0" y="6" width="4" height="4"></rect>
                        <rect x="6" y="6" width="4" height="4"></rect>
                        <rect x="12" y="6" width="4" height="4"></rect>
                    </svg>
                </a>
                <a href="{{ route('shop', route_params(['col' => 2])) }}" class="btn-layout @if(request()->get('col') == 2) active @endif">
                    <svg width="10" height="10">
                        <rect x="0" y="0" width="4" height="4"></rect>
                        <rect x="6" y="0" width="4" height="4"></rect>
                        <rect x="0" y="6" width="4" height="4"></rect>
                        <rect x="6" y="6" width="4" height="4"></rect>
                    </svg>
                </a>
                <a href="{{ route('shop', route_params(['col' => 1])) }}" class="btn-layout @if(request()->get('col') == 1) active @endif">
                    <svg width="16" height="10">
                        <rect x="0" y="0" width="4" height="4"></rect>
                        <rect x="6" y="0" width="10" height="4"></rect>
                        <rect x="0" y="6" width="4" height="4"></rect>
                        <rect x="6" y="6" width="10" height="4"></rect>
                    </svg>
                </a>
            </div>
        </div><!-- End .toolbox-right -->
    </div>
</div>
@push("scripts")
    <script>
        function setQueryStringParameter(name, value) {
            const params = new URLSearchParams(window.location.search);
            params.set(name, value);
            window.history.replaceState({}, "", decodeURIComponent(`${window.location.pathname}?${params}`));
            window.location.reload();
        }
    </script>
@endpush
