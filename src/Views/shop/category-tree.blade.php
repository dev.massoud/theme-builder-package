<?php /**  @var \WBuilder\Core\Types\ListOfCategories $items */ ?>
<ul>
    @if($items->count() > 0)
        @foreach ($items->all() as $item)
            <li data-id="{{$item->id}}" class="jstree-open" @if(@$selected && in_array($item->id, $selected)) data-jstree='{"selected":true}' @endif>{{$item->title}}
            <ul>
                @if($item->children->count() > 0)
                    @foreach ($item->children as $childItems)
                        @include('shop.category-sub-tree', ['sub_items' => $childItems])
                    @endforeach
                @endif
            </ul>
            </li>
        @endforeach
    @endif
</ul>
