<?php /**  @var \WBuilder\Core\Types\ListOfCategories $categories */ ?>
@php($categories = builder()->categories())
@if($categories->count() > 0)
<div class="widget">
    <a class="dropdown-toggle" data-toggle="collapse" href="#collapse-widget-1" role="button" aria-expanded="false" aria-controls="collapse-widget-1">
        {{__('content.product.categories')}}
    </a>
    <div class="collapse show" id="collapse-widget-1">
        <div class="card card-body">
            <div class="product-category" id="filter_category_tree">
                @include("builder/shop.category-tree", ['items' => $categories, 'selected' => explode(",", request()->get('c'))])
            </div>
            <input type="hidden" name="category_ids" value="" />
        </div>
    </div>
</div>
@endif
<div class="widget">
    <a class="dropdown-toggle" data-toggle="collapse" href="#collapse-widget-2" role="button" aria-expanded="false" aria-controls="collapse-widget-2">
        {{__('content.product.price')}}
    </a>
    <div class="collapse show" id="collapse-widget-2">
        <div class="card card-body">
            <div class="range-slider">
                <div class="box">
                    <input type="number" name="price_from" value="{{(request()->has('pf'))?request()->get('pf'):"0"}}" min="0" max="120000"/>
                    <input type="number" name="price_to" value="{{(request()->has('pt'))?request()->get('pt'):"120000"}}" min="0" max="120000"/>
                </div>
                <input value="{{(request()->has('pf'))?request()->get('pf'):"0"}}" min="0" max="120000" step="500" type="range"/>
                <input value="{{(request()->has('pt'))?request()->get('pt'):"120000"}}" min="0" max="120000" step="500" type="range"/>
            </div>
        </div>
    </div>
</div>
@php($colors = builder()->colors())
@if($colors->count() > 0)
<div class="widget">
    <a class="dropdown-toggle" data-toggle="collapse" href="#collapse-widget-3" role="button" aria-expanded="false" aria-controls="collapse-widget-3">
        {{__('content.filter.by.color')}}
    </a>
    <div class="collapse show" id="collapse-widget-3">
        <div class="card card-body">
            <div class="color-range">
                <ul class="unorder-list">
                    @php($color_ids = (request()->has('co'))?explode(",", request()->get('co')):[])
                    <?php /** @var \WBuilder\Core\Models\ProductColor $color */ ?>
                    @foreach($colors->all() as $color)
                        <li class="list-item">
                            <div class="checkbox">
                                <label><input type="checkbox" class="chk_color_filter" name="color_id" value="{{$color->color}}" @if(in_array($color->color, $color_ids)) checked @endif>{{$color->title}}</label>
                            </div>
                        </li>
                    @endforeach
                    <input type="hidden" name="color_ids" value="{{request()->get('co')}}" />
                </ul>
            </div>
        </div>
    </div>
</div>
@endif
@php($sizes = builder()->sizes())
@if($sizes->count() > 0)
<div class="widget">
    <a class="dropdown-toggle" data-toggle="collapse" href="#collapse-widget-3" role="button" aria-expanded="false" aria-controls="collapse-widget-3">
        {{__('content.filter.by.size')}}
    </a>
    <div class="collapse show" id="collapse-widget-3">
        <div class="card card-body">
            <div class="color-range">
                <ul class="unorder-list">
                    @php($sizes_id = (request()->has('s'))?explode(",", request()->get('s')):[])
                    @foreach($sizes->all() as $size)
                        <li class="list-item">
                            <div class="checkbox">
                                <label><input type="checkbox" class="chk_size_filter" name="size_id" value="{{$size->size}}" @if(in_array($size->size, $sizes_id)) checked @endif>{{$size->size}}</label>
                            </div>
                        </li>
                    @endforeach
                    <input type="hidden" name="size_ids" value="{{request()->get('s')}}" />
                </ul>
            </div>
        </div>
    </div>
</div>
@endif
<div class="widget">
    <button class="btn btn-secondary btn-lg btn-filter" style="width: 100%;">Search</button>
</div>

