@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "title" => "Cart",
        "sub_title" => "Shopping Cart",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Cart")
        )
    ])
    @include("builder/cart/index")
@endsection
@section("modals")

@endsection
