@extends("builder.profile")

@section("profile_content")
    @if(request()->route('type') == "billing")
        @include("builder/profile.billing.index")
    @else
        @include("builder/profile.shipping.index")
    @endif


@endsection
@push("profile_header")
    @include("builder/layouts/page-header", [
        "title" => "Dashboard",
        "sub_title" => "Billing Address",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => __('content.home')),
            array("url" => route('profile', route_params()), "title" => __('content.dashboard')),
            array("url" => "", "title" => "Dashboard")
        )
    ])
@endpush


