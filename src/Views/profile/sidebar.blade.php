<div class="checkoutd-nav">
    <ul class="nav flex-column nav-pills mb-3">
        <li class="nav-item">
            <a class="nav-link @if(@$form == "profile" || !@$form) active @endif" href="{{route('profile', route_params())}}">{{__('content.profile')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$form == "orders") active @endif" href="{{route('profile-orders', route_params())}}">{{__('content.orders')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$form == "address" && request()->route('type') == "shipping") active @endif" href="{{route('profile-address', route_params(['type' => "shipping"]))}}">{{__('content.shipping.addresses')}}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(@$form == "address" && request()->route('type') == "billing") active @endif" href="{{route('profile-address', route_params(['type' => "billing"]))}}">{{__('content.billing.addresses')}}</a>
        </li>

        <li class="nav-item">
            <a class="nav-link " href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-sidebar-form').submit();">{{__('content.logout')}}</a>
        </li>
    </ul>
    <form id="logout-sidebar-form" action="{{ route('logout', route_params()) }}" method="POST" class="d-none">
        @csrf
    </form>
</div>
