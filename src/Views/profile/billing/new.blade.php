@extends("builder.profile")
@section("profile_content")
    <h3 class="">{{(request()->has('id'))?__('content.update.address.title'):__('content.add.address.title')}} </h3>
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{session('success')}}
        </div>
    @endif
    <form method="POST" action="{{route('new-profile-address', route_params(['f' => 'address-form', 'type' => request()->route('type'), 'id' => request()->get('id')]))}}">
        @csrf
        <div class="form-row">
            <div class="from-group col-md-6 mb-3">
                <div class="input-group ">
                    <input type="text" class="form-control @error("title") is-invalid @enderror" id="title" name="title"
                           placeholder="{{__('content.title_text')}}"
                           value="{{(old("title", (@$address_info)?($address_info->title):("")))}}"
                    >
                    @error("title")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="from-group col-md-6 mb-3">
                <div class="input-group"  >
                    <input type="text" class="form-control @error("email") is-invalid @enderror" id="email" name="email"
                           placeholder="{{__('content.email')}}"
                           value="{{(old('email', ((@$address_info)?($address_info->email):(""))))}}"
                    >
                    @error("email")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="from-group col-md-6 mb-3">
                <div class="input-group ">
                    <input type="text" class="form-control @error("firstname") is-invalid @enderror" id="firstname"
                           name="firstname"
                           placeholder="{{__('content.firstname')}}"
                           value="{{(old('firstname', ((@$address_info)?($address_info->firstname):(""))))}}"
                    >
                    @error("firstname")
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>
            </div>
            <div class="from-group col-md-6 mb-3">
                <div class="input-group ">
                    <input type="text" class="form-control @error("lastname") is-invalid @enderror" id="lastname"
                           name="lastname"
                           placeholder="{{__('content.lastname')}}"
                           value="{{(old('lastname', ((@$address_info)?($address_info->lastname):(""))))}}"
                    >
                    @error("lastname")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="from-group col-md-12 mb-3">
                <div class="input-group ">
                    <input type="text" class="form-control @error("address") is-invalid @enderror" id="address" name="address"
                           placeholder="{{__('content.address')}}"
                           value="{{(old('address', ((@$address_info)?($address_info->address):(""))))}}"
                    >
                    @error("address")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="from-group col-md-4 mb-3">
                <div class="input-group select-control">
                    <select class="form-control dd_countries @error("country") is-invalid @enderror" id="country" name="country">
                        <option value="" selected>{{__('content.select.country')}}</option>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}" @if((old('country', ((@$address_info)?($address_info->country->id):(""))) == $country->id)) selected @endif>{{$country->name}}</option>
                        @endforeach
                    </select>
                    @error("country")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="from-group col-md-4 mb-3">
                <div class="input-group select-control">
                    <select class="form-control dd_states @error("state") is-invalid @enderror" id="state" name="state">
                        <option value="" selected>{{__('content.select.state')}}</option>
                        @if($states = (session('states'))?session('states'):$states)
                            @foreach($states as $state)
                                <option value="{{$state->id}}" @if((old('state', ((@$address_info)?($address_info->state->id):(""))) == $state->id)) selected @endif>{{$state->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    @error("state")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="from-group col-md-4 mb-3">
                <div class="input-group select-control">
                    <select class="form-control dd_cities @error("city") is-invalid @enderror" id="city" name="city">
                        <option value="" selected>{{__('content.select.city')}}</option>
                        @if($cities = (session('cities'))?session('cities'):$cities)
                            @foreach($cities as $city)
                                <option value="{{$city->id}}" @if((old('city', ((@$address_info)?($address_info->city->id):(""))) == $city->id)) selected @endif>{{$city->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    @error("city")
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>
            </div>

        </div>
        <div class="form-row">
            <div class="from-group col-md-6 mb-3">
                <div class="input-group"  >
                    <input type="text" class="form-control @error("postal_code") is-invalid @enderror" id="postal_code"
                           name="postal_code"
                           placeholder="{{__('content.postal.code')}}"
                           value="{{(old('postal_code', ((@$address_info)?($address_info->postal_code):(""))))}}"
                    >
                    @error("postal_code")
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

        </div>
        <div class="col-12 col-sm-12 justify-content-end btn-cont">
            <div class="row">
                <button type="submit" class="btn btn-secondary">{{(request()->has('id'))?__('content.update'):__('content.add')}}</button>
            </div>
        </div>
    </form>
@endsection
@push("profile_header")
    @include("builder/layouts/page-header", [
        "title" => "Dashboard",
        "sub_title" => (request()->get('id'))?"Edit Billing Address":"New Billing Address",
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => route('profile', route_params()), "title" => "Dashboard"),
            array("url" => route('profile-address', route_params(['type' => request()->route('type')])), "title" => "Billing Addresses"),
            array("url" => "", "title" => (request()->get('id'))?"Edit Billing Address":"New Billing Address")
        )
    ])
@endpush


