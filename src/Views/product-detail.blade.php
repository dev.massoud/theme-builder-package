@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "breadcrumb" => array(
            array("url" => route('web-home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Wishlist")
        )
    ])
    @include("builder/product-detail/index")
@endsection
@section("modals")

@endsection
