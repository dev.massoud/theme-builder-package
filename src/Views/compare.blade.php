@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        "title" => "Compare",
        "sub_title" => "Compare Products Overview",
        "breadcrumb" => array(
            array("url" => route('home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Compare")
        )
    ])
    @include("builder/compare/index")
@endsection
@section("modals")

@endsection
