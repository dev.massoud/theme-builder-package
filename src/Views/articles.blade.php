@extends("builder/layouts/master")
@section("content")
    @include("builder/layouts/page-header", [
        'title' => "Blog",
        'sub_title' => 'Stay upto date',
        "breadcrumb" => array(
            array("url" => route('home', route_params()), "title" => "Home"),
            array("url" => "", "title" => "Blog")
        )
    ])
    @include("builder/articles/index")
@endsection
@section("modals")

@endsection
