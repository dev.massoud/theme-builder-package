<header id="stickyHeader" class="header-area header-sticky d-none  bg-sticky-bar" >
    <div class="container">

        <div class="row align-items-center">
            <div class="col-12 col-xl-2 col-lg-1">
                <div class="logo">
                    @if($logo = builder()->key('logo_dark'))
                        <a href="{{route('web-home', route_params())}}" >
                            <img class="img-fluid ref_elm_logo_dark" src="{{ builder_image_url($logo) }}" alt="logo here" />
                        </a>
                    @endif
                </div>
            </div>
            <div class="col-12 col-lg-7 col-xl-6">
                @include("builder/layouts/menu")
            </div>
            <div class="col-12 col-lg-4 d-flex justify-content-end">
                <div class="navbar-lang">
                    <ul>
                        @include("builder/layouts/language")
                        @include("builder/layouts/currency")
                    </ul>
                </div>
                @include("builder/layouts/search")
            </div>
        </div>
    </div>
</header>
