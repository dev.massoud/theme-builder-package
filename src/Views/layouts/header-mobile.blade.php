<header id="headerMobile" class="header-area header-mobile">

    <div class="header-maxi bg-header-bar ">
        <div class="container">

            <div class="row align-items-center">
                <div class="col-6 pr-0 flex-col">
                    <div class="navigation-mobile-container">
                        <a href="javascript:void(0)" class="navigation-mobile-toggler">
                            <span class="fas fa-bars"></span>
                        </a>
                        @include("builder/layouts/menu-mobile")
                    </div>
                    @if($logo = builder()->key('logo_dark'))
                        <a href="{{route('web-home', route_params())}}" class="logo">
                            <img class="img-fluid ref_elm_logo_dark" src="{{ builder_image_url($logo) }}" alt="logo here" />
                        </a>
                    @endif
                </div>



                <div class="col-6 pl-0">
                    <ul class="pro-header-right-options">
                        <li>
                            <a href="{{route('wishlist', route_params())}}" class="btn btn-light" >
                                <i class="far fa-heart"></i>
                                <span class="badge badge-secondary">0</span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <button class="btn btn-light dropdown-toggle" type="button" id="dropdownCartButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-shopping-bag"></i>
                                <span class="badge badge-secondary">2</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownCartButton2">
                                <ul class="shopping-cart-items">
                                    <li>
                                        <div class="item-thumb">

                                            <div class="image">
                                                <img class="img-fluid" src="{{ asset('assets/images/product_images/product_image_1.jpg') }}" alt="Product Image">
                                            </div>
                                        </div>
                                        <div class="item-detail">
                                            <h3>Modern Single Sofa</h3>
                                            <div class="item-s">1 x $45.00 <i class="fas fa-trash"></i></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="item-thumb">

                                            <div class="image">
                                                <img class="img-fluid" src="{{ asset('assets/images/product_images/product_image_2.jpg') }}" alt="Product Image">
                                            </div>
                                        </div>
                                        <div class="item-detail">
                                            <h3>Modern Wood Chair</h3>
                                            <span class="item-s">2 x $90.00 <i class="fas fa-trash"></i></span>
                                        </div>
                                    </li>
                                    <li>
                                          <span class="item-summary">Total&nbsp;:&nbsp;<span>$145.00</span>
                                          </span>
                                    </li>
                                    <li>
                                        <a class="btn btn-link btn-block " href="{{route('cart', route_params())}}">View Cart</a>
                                        <a class="btn btn-secondary btn-block  " href="{{route('checkout', route_params())}}">Checkout</a>
                                    </li>
                                </ul>


                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-navbar bg-menu-bar">
        <div class="container">
            <form class="form-inline">
                <div class="search-field-module">

                    <div class="search-field-wrap">
                        <input  type="search" placeholder="Search Products..." data-toggle="tooltip" data-placement="bottom" title="search item">
                        <button class="btn btn-secondary swipe-to-top" data-toggle="tooltip" data-placement="bottom" title="Search Products">
                            <i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</header>
