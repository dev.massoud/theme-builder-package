@if($currencies = builder()->currencies())
@if(count($currencies) > 1)
<li class="dropdown">
    <button class="btn dropdown-toggle" type="button" >
        {{strtoupper(builder()->getDefaultCurrency()->code)}}
    </button>

    <div class="dropdown-menu  dropdown-menu-right">
        @foreach($currencies as $currency)
            <a class="dropdown-item" href="#">{{$currency->code}}</a>
        @endforeach

    </div>
</li>
@endif
@endif
