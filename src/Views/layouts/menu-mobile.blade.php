<nav id="navigation-mobile">
    <div class="logout-main">
        <div class="welcome">
            <span>{{ __('content.welcome.guest') }}</span>
        </div>
    </div>

    <a class="main-manu btn" href="{{ route('web-home', route_params()) }}">
        {{ __('content.home') }}
    </a>
    <a class="main-manu btn" href="{{ route('shop', route_params()) }}">
        {{ __('content.products') }}
    </a>
    <a class="main-manu btn" href="{{ route('articles-list', route_params()) }}">
        {{ __('content.news') }}
    </a>
    <a class="main-manu btn" href="{{ route('articles-list', route_params()) }}">
        {{ __('content.contact') }}
    </a>
    <a class="main-manu btn" href="{{ route('articles-list', route_params()) }}">
        {{ __('content.about') }}
    </a>

    <a href="{{route('profile', route_params())}}" class="main-manu btn ">
        {{ __('content.my.account') }}
    </a>
    <a href="{{route('wishlist', route_params())}}" class="main-manu btn ">
        {{ __('content.wishlist') }} (8)
    </a>
    <a href="logiut.html" class="main-manu btn ">
        {{ __('content.logout') }}
    </a>
</nav>
