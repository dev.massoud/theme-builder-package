<nav class="navbar navbar-expand-lg">
    <div class="navbar-collapse" >
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="{{route('web-home', route_params())}}">
                    {{__('content.home')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('shop', route_params())}}">
                    {{__('content.products')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('articles-list', route_params())}}">
                    {{__('content.news')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('articles-list', route_params())}}">
                    {{__('content.contact')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('articles-list', route_params())}}">
                    {{__('content.about')}}
                </a>
            </li>
            @if (!\Illuminate\Support\Facades\Auth::guard('web')->check())
                <li class="nav-item">
                    <a class="nav-link" href="{{route('register', route_params())}}">
                        {{__('content.login')}}/{{__('content.register')}}
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>
