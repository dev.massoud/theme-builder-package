<div class="modal fade show" id="newsletterModal" tabindex="-1" role="dialog" aria-hidden="false">

    <div class="modal-dialog modal-dialog-centered modal-lg newsletter" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="container">
                    <div class="row align-items-center">


                        <div class="col-12 col-md-6" >
                            <div class="pro-image">
                                <img class="img-fluid" src="{{ asset('assets/images/miscellaneous/newslater.jpg') }}" alt="blogImage">
                            </div>
                        </div>
                        <div class="col-12 col-md-6" >
                            <div class="promo-box">

                                <h2 class="text-01">
                                    Sign Up for Our Newsletter
                                </h2>
                                <p class="text-03">
                                    Be the first to learn about our latest trends and get exclusive offers.
                                </p>





                                <form>
                                    <div class="form-group">
                                        <input type="email" value="" name="EMAIL" class="required email form-control" placeholder="Enter Your Email Address...">

                                    </div>
                                    <div class="form-check">

                                        <input class="form-check-input" type="checkbox" value="" id="defaultCheck0">
                                        <label class="form-check-label" for="defaultCheck0">
                                            Do Not Show This Popup Again
                                        </label>
                                        <small id="checkboxHelp" class="form-text text-muted"></small>
                                    </div>
                                    <button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-secondary swipe-to-top">Subscribe</button>
                                </form>



                            </div>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
