<ul>
    @foreach($children as $child)
        <li class="list-item">
            <div class="checkbox">
                <label><input type="checkbox" value="" checked>{{$settings->translations($child, 'title')}}</label>
            </div>
            @if(count($child->children))
                @include('builder.layouts.child',['children' => $child->children])
            @endif
        </li>
    @endforeach
</ul>
