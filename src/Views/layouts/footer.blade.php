<footer id="footerOne"  class="footer-area footer-one footer-desktop">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 newsletter">
                <h5>{{__('content.newsletter')}}</h5>
                @if($newsletter_title = builder()->key('newsletter_title'))
                    <h3 class="ref_elm_newsletter_title">{{$newsletter_title}}</h3>
                @endif
                @if($newsletter_description = builder()->key('newsletter_description'))
                    <p class="ref_elm_newsletter_description">{!! nl2br($newsletter_description) !!}</p>
                @endif
                <div class="input-group">
                    <input type="email" class="form-control" placeholder="{{__('content.enter.your.email')}}..." aria-label="{{__('content.enter.your.email')}}..." aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" id="button-addon2">{{__('content.submit')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="copyright-content">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="footer-info">
                            @php($company_name = builder()->key('company_name'))
                            ©&nbsp;2019 <span class="ref_elm_company_name">{{$company_name}}</span>, Inc. <a href="privacy.html">Privacy</a>&nbsp;•&nbsp;<a href="term.html">Terms</a>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 ref_elm_social_media">
                        @if($social_media_items = builder()->key('social_media'))
                            <ul class="socials ref_elm_container">
                                @foreach($social_media_items as $social_media_index => $social_media_item)
                                    <li>
                                        <a href="{{$social_media_item['url']}}" target="_blank" class="{{$social_media_item['icon']}}"></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                        <template>
                            <li>
                                <a href="@URL@" target="_blank" class="@ICON@"></a>
                            </li>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
