<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-js">
    <head>
        <meta charset="UTF-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @php($website_title = builder()->key('website_title'))
        <title>{{$website_title}}</title>
        @php($website_description = builder()->key('website_description'))
        <meta name="description" content="{{$website_description}}">
        @php($website_keywords = builder()->key('website_keywords'))
        <meta name="keywords" content="{{$website_keywords}}">

        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" type="image/png" href="{{ asset('assets/images/miscellaneous/fav.png') }}">

        <!-- Fontawesome CSS Files -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
        <!-- Core CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

        <!-- Slider Revolution CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/settings.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/layers.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/revolution/css/navigation.css') }}">
    </head>
    <body>
{{--    @if (Route::has('login'))--}}
{{--        <div class="top-right links">--}}
{{--            @if (Auth::check())--}}
{{--                <a href="{{ url('/home') }}">Home</a>--}}
{{--            @else--}}
{{--                <a href="{{ url('/login') }}">Login</a>--}}
{{--                <a href="{{ url('/register') }}">Register</a>--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    @endif--}}

        @include("builder/layouts/preloading")
        <div class="wrapper" style="display: none;">
            @include("builder/layouts/header")
            @include("builder/layouts/header-sticky")
            @include("builder/layouts/header-mobile")
            @yield('content')
            @include("builder/layouts/footer")

        </div>
        <div class="mobile-overlay"></div>
        <a href="#" class="btn-secondary " id="back-to-top" title="Back to top">&uarr;</a>


        <div class="notifications" id="notificationWishlist">Product Added To Wishlist</div>
        <div class="notifications" id="deletedNotificationWishlist">Product Deleted From Wishlist</div>
        <div class="notifications" id="notificationCart">Product Added To Cart</div>
        <div class="notifications" id="notificationCompare">Product Added For Compare</div>

        <template id="tmp_loading">
            <div class="spinner0"></div>
        </template>
        @yield('modals')
        <script>
            let CONFIG_ROUTE_ADD_TO_CART = '{{route('add-to-cart', route_params())}}';
            let CONFIG_ROUTE_DELETE_FROM_CART = '{{route('delete-cart-item', route_params())}}';
            let CONFIG_ROUTE_GET_STATES = '{{route('get-states', route_params())}}';
            let CONFIG_ROUTE_GET_CITIES = '{{route('get-cities', route_params())}}';
            let CONFIG_ROUTE_ADD_TO_FAVORITE = '{{route('add-to-favorite', route_params())}}';
            let CONFIG_ROUTE_GENERATE_PRODUCT_HTML = '{{route('generate-product-list', route_params())}}';
            let CONFIG_ROUTE_GENERATE_PRODUCT_COLOR_HTML = '{{route('generate-product-colors', route_params())}}';
            let CONFIG_ROUTE_GENERATE_PRODUCT_SIZE_HTML = '{{route('generate-product-sizes', route_params())}}';
            let CONFIG_ROUTE_GENERATE_SHOP_HTML = '{{route('generate-shop', route_params())}}';


            let LANG_PLEASE_SELECT_STATE = '{{__('content.select.state')}}';
            let LANG_PLEASE_SELECT_CITY = '{{__('content.select.city')}}';
            let LANG_ADD_TO_FAVORITE = '{{__('content.add.to.wishlist')}}';
            let LANG_DELETE_FROM_FAVORITE = '{{__('content.delete.from.wishlist')}}';

            let CONFIG_CURRENT_CURRENCY = '{!! json_encode(builder()->getDefaultCurrency()) !!}'
        </script>
        <!-- All custom scripts here -->
        <script src="{{ asset('assets/js/scripts.js') }}"></script>

        <!-- Slider Revolution core JavaScript files -->
        <script src="{{ asset('assets/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

        <!-- Slider Revolution extension scripts. ONLY NEEDED FOR LOCAL TESTING -->
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
        <script src="{{ asset('assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script type="text/javascript">
            (function (window, document, $) {
                function nl2br (str, is_xhtml) {
                    if (typeof str === 'undefined' || str === null) {
                        return '';
                    }
                    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                }
                let config = {
                    origin: "{{env('BUILDER_CORS_DOMAIN')}}"
                }
                let eventMethod = window.addEventListener
                    ? "addEventListener"
                    : "attachEvent";
                let eventer = window[eventMethod];
                let messageEvent = eventMethod === "attachEvent"
                    ? "onmessage"
                    : "message";
                eventer(messageEvent, function (e) {
                    if(e.origin !== config.origin) return false;
                    let data = JSON.parse(e.data);
                    let ref = '.ref_elm_'+data.ref;
                    // if(data.ref === "slideshow"){
                    //     window.location.reload();
                    // }
                    if(data.action === "update_html"){
                        $(ref).each(function (){
                            $(this).html(nl2br(data.data));
                        })

                    }
                    if(data.action === "update_text"){

                        $(ref).each(function (){
                            $(this).text((data.data));
                        })
                    }
                    if(data.action === "update_price"){
                        let cur = JSON.parse(CONFIG_CURRENT_CURRENCY);
                        if(data.data.currency.id === cur.id){
                            $(ref).each(function (){
                                if($(this).find('ins').length > 0){
                                    let price = data.data.total + data.data.currency.symbol;
                                    if(parseFloat(data.data.discount) > 0){
                                        price += `<del>${data.data.price}${data.data.currency.symbol}</del>`
                                    }
                                    $($(this).find('ins')[0]).html(price)
                                }
                            })
                        }

                    }
                    if(data.action === "update_colors"){
                        //ref_elm_8_product_colors
                        (function (window, document, $, undefined){
                            $.ajax({
                                url: CONFIG_ROUTE_GENERATE_PRODUCT_COLOR_HTML,
                                type: "POST",
                                data: {
                                    product: JSON.stringify(data.data.product),
                                    colors: JSON.stringify(data.data.colors),
                                    template: $(ref).data('template')
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(response){
                                    $(ref).html(response.html);

                                }
                            });
                        }(window, document, jQuery));
                    }
                    if(data.action === "update_sizes"){
                        //ref_elm_8_product_colors
                        (function (window, document, $, undefined){
                            $.ajax({
                                url: CONFIG_ROUTE_GENERATE_PRODUCT_SIZE_HTML,
                                type: "POST",
                                data: {
                                    product: JSON.stringify(data.data.product),
                                    sizes: JSON.stringify(data.data.sizes),
                                    template: $(ref).data('template')
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(response){
                                    $(ref).html(response.html);

                                }
                            });
                        }(window, document, jQuery));
                    }
                    if(data.action === "update_image"){
                        if($(ref).length > 0){
                            $(ref).each(function (){
                                if($(this).prop('tagName').toLowerCase() === "img"){
                                    $(this).attr('src', (data.data));
                                }else{
                                    $(this).css('background-image', `url('${data.data}')`);
                                }
                            })
                        }else if($(`.ref_elm_post_${data.id}_slideshow`).length > 0){
                            ref = $(`.ref_elm_post_${data.id}_slideshow div.tp-bgimg`);
                            $(ref).each(function (){
                                $(this).css('background-image', 'url("' + data.data + '")')
                                    .attr('src', data.data)
                                    .data('src', data.data);
                            })
                        }
                    }
                    if(data.action === "update_post_image"){
                        $(ref).each(function (){
                            if($(this).prop('tagName').toLowerCase() === "img"){
                                $(this).attr('src', (data.data));
                            }else{
                                $(this).css('background-image', `url('${data.data}')`);
                            }
                        })
                    }

                    if(data.action === "update_social"){
                        $(ref).each(function (){
                            let template = $(this).find('template');
                            if(template.length > 0){
                                $(ref+" .ref_elm_container").html("");
                                data.data.map((m) => {
                                    let html = $(template[0]).html();
                                    html = html.replaceAll("@URL@", m.url).replaceAll("@ID@", m.id).replaceAll("@ICON@", m.icon);
                                    $(ref+" .ref_elm_container").append($(html))
                                })

                            }
                        })
                    }
                    if(data.action === "update_menu"){
                        $(ref).each(function (){
                            let template = $(this).find('template');

                            if(template.length > 0){
                                $(ref+" .ref_elm_container").html("");
                                data.data.map((m) => {
                                    let html = $(template[0]).html();
                                    html = html.replaceAll("@GOTO@", m.goto).replaceAll("@TITLE@", m.title);
                                    $(ref+" .ref_elm_container").append($(html))
                                })

                            }
                        })
                    }
                    if(data.action === "update_post_list"){

                        $(ref).each(function (){
                            let template = $(this).find('template');
                            if(template.length > 0){
                                $(ref+" .ref_elm_container").html("");
                                data.data.map((m) => {
                                    let html = $(template[0]).html();
                                    html = html
                                        .replaceAll("@ID@", m.id)
                                        .replaceAll("@IMAGE@", (m.media.length > 0)?m.media[0].path:"")
                                        .replaceAll("@TITLE@", m.title)
                                        .replaceAll("@SHORT_CONTENT@", m.short_content)
                                        .replaceAll("@LONG_CONTENT@", m.long_content)
                                        .replaceAll("@URL@", (m.additional_data && m.additional_data.url)?m.additional_data.url:"")
                                        .replaceAll("@PRICE@", (m.price)?m.price:"")
                                    ;
                                    $(ref+" .ref_elm_container").append($(html))
                                })

                            }
                            if($(ref+" .ref_elm_container").hasClass("owl-carousel")){
                                let $owl = $(`#${$(ref+" .ref_elm_container").attr('id')}`);
                                $owl.trigger('destroy.owl.carousel');
                                $owl.find('.owl-stage-outer').children().unwrap();
                                $owl.removeClass("owl-center owl-loaded owl-text-select-on");
                                $owl.owlCarousel();
                            }
                        })
                    }

                    if(data.action === "update_section_status"){
                        $(ref).each(function (){
                            if(data.data){
                                $(this).hide();
                            }else{
                                $(this).show();
                            }

                        })
                    }
                    if(data.action === "update_image_group"){
                        $(ref).each(function (){
                            if($(this).prop('tagName').toLowerCase() === "img"){
                                $(this).attr('src', (data.data));
                            }else{
                                $(this).css('background-image', `url('${data.data}')`);
                            }
                        })
                    }
                    if(data.action === "update_product_list"){
                        //
                        (function (window, document, $, undefined){
                            $.ajax({
                                url: CONFIG_ROUTE_GENERATE_PRODUCT_HTML,
                                type: "POST",
                                data: {
                                    products: JSON.stringify(data.data),
                                    template: $(ref+" .ref_elm_container").data('template')
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(response){
                                    $(ref+" .ref_elm_container").slick("unslick");
                                    $(ref+" .ref_elm_container .slick-list").remove();
                                    $(ref+" .ref_elm_container .slick-dots").remove();
                                    $(ref+" .ref_elm_container").html(response.html);
                                    if($(ref+" .ref_elm_container").hasClass('tab-carousel-js')){
                                        getTabSliderSettings($(ref+" .ref_elm_container"))
                                    }
                                    if($(ref+" .ref_elm_container").hasClass('flashsale-carousel-js')){
                                        LoadFlashSaleCarouselSettings($(ref+" .ref_elm_container"))
                                    }


                                }
                            });
                        }(window, document, jQuery));
                    }
                    if(data.action === "update_shop_list"){
                        (function (window, document, $, undefined){
                            $.ajax({
                                url: CONFIG_ROUTE_GENERATE_SHOP_HTML,
                                type: "POST",
                                data: {
                                    products: JSON.stringify(data.data),
                                    template: $(ref+" .ref_elm_container").data('template')
                                },
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function(response){
                                    $(ref+" .ref_elm_container").html(response.html);
                                }
                            });
                        }(window, document, jQuery));
                    }
                    if(data.action === "group_action_page"){

                        if(data.data.type && data.data.type === "scroll"){
                            console.log(data.data)
                            $('body,html').stop(true).animate({
                                'scrollTop': $(`#${data.data.url}`).offset().top
                            }, 800, "easeInOutExpo" )

                        }
                        if(data.data.type && data.data.type === "redirect"){
                            let newURL = window.location.protocol + "//" + window.location.host + "/website/preview" + data.data.url + window.location.search;
                            window.location.href = newURL
                        }

                    }
                });
            }(window, document, jQuery));

        </script>
        @stack("scripts")
    </body>
</html>
