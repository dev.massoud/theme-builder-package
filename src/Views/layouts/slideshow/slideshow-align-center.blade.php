<li class="ref_elm_post_{{$id}}_slideshow" data-index="rs-{{$id}}" data-align="center" data-transition="slideoverhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000" data-thumb=""  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Big &amp; Bold" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
    <img style="visibility: hidden" src="{{ @$image }}"/>
    <img src="{{ @$image }}" alt=""  data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

    <div class="container" style="position: relative;">
        @if(@$title)
        <!-- LAYER NR. 1 -->
        <div class="tp-caption BigBold-SubTitle ref_elm_post_{{$id}}_title"
             id="slide-3043-layer-{{mt_rand(111111, 999999)}}"
             data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['150','90','60','40']"
             data-fontsize="['18','18','18','14']"
             data-width="['100%','100%','100%','100%']"
             data-height="none"
             data-whitespace="normal"

             data-type="text"
             data-basealign="slide"
             data-responsive_offset="off"
             data-responsive="off"
             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;",
                    "speed":1500,"to":"o:1;","delay":650,"ease":"Power3.easeInOut"},
                    {"delay":"wait","speed":1000,"to":"y:50px;opacity:0;","ease":"Power2.easeInOut"}]'
             data-textAlign="['center','center','center','center']"
             data-paddingtop="[0,0,0,0]"
             data-paddingright="[0,0,0,0]"
             data-paddingbottom="[0,0,0,0]"
             data-paddingleft="[0,0,0,0]"

             style="z-index: 12;  white-space: normal; line-height: 1.5;">{{$title}}</div>
        @endif

        <!-- LAYER NR. 2 -->
        @if(@$short_content)
        <div class="tp-caption BigBold-Title  tp-resizeme ref_elm_post_{{$id}}_short_content"
             id="slide-3043-layer-{{mt_rand(111111, 999999)}}"
             data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['170','120','90','70']"
             data-fontsize="['50','40','30','24']"
             data-lineheight="['70','50','40','30']"
             data-width="['100%','100%','100%','100%']"
             data-height="none"
             data-whitespace="['nowrap','nowrap','nowrap','normal']"

             data-type="text"
             data-basealign="slide"
             data-responsive_offset="off"

             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;",
                      "mask":"x:0px;y:[-100%];s:inherit;e:inherit;","speed":1500,"to":"o:1;",
                      "delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
             data-textAlign="['center','center','center','center']"
             data-paddingtop="[0,0,0,0]"
             data-paddingright="[0,0,0,0]"
             data-paddingbottom="[0,0,0,0]"
             data-paddingleft="[0,0,0,0]"

             style="z-index: 11; white-space: nowrap; color:#000; ">{!! nl2br($short_content) !!}</div>
        @endif



        <!-- LAYER NR. 3 -->
        <div class="tp-caption BigBold-Button rev-btn "
             id="slide-3043-layer-{{mt_rand(111111, 999999)}}"
             data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['330','250','200','150']"

             data-height="none"
             data-whitespace="nowrap"

             data-type="button"
             data-actions='[{"event":"click","action":"simplelink","target": "_self","url":"{{ route('shop', route_params()) }}"}]'
             data-basealign="slide"
             data-responsive_offset="off"
             data-responsive="off"
             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;",
                        "speed":1500,"to":"o:1;","delay":650,"ease":"Power3.easeInOut"},
                        {"delay":"wait","speed":1000,"to":"y:50px;opacity:0;","ease":"Power2.easeInOut"},
                        {"frame":"hover","speed":"300","ease":"Power1.easeInOut",
                        "to":"o:1;rX:0;rY:0;rZ:0;z:0;",
                        "style":"c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);bw:1px 1px 1px 1px;"}]'
             data-textAlign="['center','center','center','center']"
             data-paddingtop="[13,13,13,13]"
             data-paddingright="[25,25,25,25]"
             data-paddingbottom="[13,13,13,13]"
             data-paddingleft="[25,25,25,25]"

             style="z-index: 13; text-transform: uppercase; white-space: nowrap;outline:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer; border-radius: 3px;">{{__('content.shop.now')}}</div>

    </div>
</li>
