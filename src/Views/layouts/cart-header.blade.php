<ul class="shopping-cart-items">
    <li>
        <div class="item-thumb">
            <div class="image">
                <img class="img-fluid" src="{{ asset('assets/images/product_images/product_image_1.jpg') }}" alt="Product Image">
            </div>
        </div>
        <div class="item-detail">
            <h3>Modern Single Sofa</h3>
            <div class="item-s">1 x $45.00 <i class="fas fa-trash"></i></div>
        </div>
    </li>
    <li>
        <div class="item-thumb">
            <div class="image">
                <img class="img-fluid" src="{{ asset('assets/images/product_images/product_image_3.jpg') }}" alt="Product Image">
            </div>
        </div>
        <div class="item-detail">
            <h3>Modern Wood Chair</h3>
            <span class="item-s">2 x $90.00 <i class="fas fa-trash"></i></span>
        </div>
    </li>
    <li>
        <span class="item-summary">{{ __('content.total') }}&nbsp;:&nbsp;<span>$145.00</span></span>
    </li>
    <li>
        <a class="btn btn-link btn-block " href="{{route('cart', route_params())}}">{{ __('content.view.cart') }}</a>
        <a class="btn btn-secondary btn-block  " href="{{route('checkout', route_params())}}">{{ __('content.checkout') }}</a>
    </li>
</ul>
