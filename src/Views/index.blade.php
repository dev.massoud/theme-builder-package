@extends("builder/layouts/master")
@section("content")
    @include("builder/home/index")
@endsection
@section("modals")
    @include("builder/layouts/quick-view-modal")
@endsection
