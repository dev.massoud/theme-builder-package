<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Category;

class GetListOfCategoriesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected Category $model;

    public function getData()
    {
        $data = $this->getBaseData('/categories/list', 'GET');
        return $data;
    }

    public function parseData(){
        $categories = [];
        if($this->draft)
            $categories = $this->draft->meta_data->meta->search("categories", "meta_key");
        else
            $categories = $this->template->meta->search("categories", "meta_key");
        if($categories) return $categories->data;
        return $categories;
    }
}
