<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Common\Messages\AbstractResponse;
use WBuilder\Core\Common\Messages\RequestInterface;

/**
 * PayPal Response
 */
class Response extends AbstractResponse
{
    public function __construct(RequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = json_decode($data, true);
    }

    public function isSuccessful()
    {
        return !isset($this->data['error']);
    }

    public function getError()
    {
        return isset($this->data['error'])?$this->data['error']:null;
    }

    public function getErrorDescription()
    {
        return isset($this->data['error_description'])?$this->data['error_description']:null;
    }
}
