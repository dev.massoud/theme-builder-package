<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductSize;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfProductSize;

class GetListOfProductSizesRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected ProductSize $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/sizes', 'GET');
        return $data;
    }

    public function parseData(){
        $products = new ListOfProduct();
        $sizes = new ListOfProductSize();
        if($this->draft){
            $products = $this->draft->meta_data->meta->search('products', 'meta_key')->data;
        }else{
            $products = $this->template->meta->search('products', 'meta_key')->data;
        }
        /** @var Product $product */
        foreach ($products->all() as $product){
            if($product->sizes->count() > 0){
                foreach ($product->sizes->all() as $size){
                    if(!$sizes->search($size->id))
                        $sizes->insert($size);
                }
            }
        }
        return $sizes;
    }
}
