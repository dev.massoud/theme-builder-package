<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Currency;

class GetCurrenciesRequest extends AbstractRequest
{

    protected $data_type = 'list';
    protected Currency $model;

    public function getData()
    {
        $data = $this->getBaseData('/common/currencies', 'GET');
        return $data;
    }

    public function parseData(){
        $output = [];
        if($this->draft)
            $output = $this->draft->meta_data->currencies;
        else
            $output = array($this->template->currency);

        return $output;
    }
}
