<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfProduct;

class GetProductsDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ProductPaginate $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/search', 'GET');
        return $data;
    }

    public function parseData(){
        $records = new ListOfProduct();
        if($this->draft){
            $records = $this->draft->meta_data->meta->search('products', 'meta_key')->data;
        }else{
            $records = $this->template->meta->search('products', 'meta_key')->data;
        }
        if($keyword = $this->getParameter('keyword')){
            $records = $records->collect()->filter(function ($item) use ($keyword) {
                return false !== stristr($item->title, $keyword);
            });
            $records = builder_put_data_to_list($records, ListOfProduct::class);
        }
        if($this->getParameter('categories')){
            $categories = explode(",", $this->getParameter('categories'));
            $records = $records->collect()->filter(function ($item) use ($categories) {
                return $item->categories->has($categories, 'category_id')->count() > 0;
            });
            $records = builder_put_data_to_list($records, ListOfProduct::class);
        }
        if($this->getParameter('colors')){
            $colors = explode(",", $this->getParameter('colors'));
            $records = $records->collect()->filter(function ($item) use ($colors) {
                return $item->colors->has($colors, 'color')->count() > 0;
            });
            $records = builder_put_data_to_list($records, ListOfProduct::class);
        }
        if($this->getParameter('sizes')){
            $sizes = explode(",", $this->getParameter('sizes'));
            $records = $records->collect()->filter(function ($item) use ($sizes) {
                return $item->sizes->has($sizes, 'size')->count() > 0;
            });
            $records = builder_put_data_to_list($records, ListOfProduct::class);
        }
        $limit = ($this->getParameter('limit'))?:10;
        $page = ($this->getParameter('page'))?:1;
        $sort_field = ($this->getParameter('sort_field'))?:'id';
        $sort_type = ($this->getParameter('sort_type'))?:'desc';
        $data = builder_pagination();
        if($records) {
            $rows = collect($records->all());
            if(strtoupper($sort_type) != "desc")
                $rows = $rows->sortBy($sort_field);
            else
                $rows = $rows->sortByDesc($sort_field);
            $rows = $rows->skip(($page - 1) * $limit)->take($limit);
            $data = builder_pagination($rows, $records->count(), $this->getParameter('limit'));
        }


        $to = $data->currentPage() * $data->perPage();
        if($to > $data->total()) $to = $data->total();
        $output['current_page'] = $data->currentPage();
        $output['from'] = (($data->currentPage() - 1) * $data->perPage()) + 1;
        $output['last_page'] = $data->lastPage();
        $output['per_page'] = $data->perPage();
        $output['to'] = $to;
        $output['total'] = $data->total();
        $output['data'] = $data->items();
        return new ProductPaginate($output);
    }
}
