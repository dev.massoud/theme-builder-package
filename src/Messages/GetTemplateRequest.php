<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Template;

class GetTemplateRequest extends AbstractRequest
{
    protected Template $model;
    public function getData()
    {
        $data = $this->getBaseData('/templates/get', 'GET');
        return $data;
    }

}
