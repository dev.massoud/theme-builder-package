<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\ProductFavoritePaginate;

class GetCustomerWishlistRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ProductFavoritePaginate $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/favorites', 'GET');
        return $data;
    }

}
