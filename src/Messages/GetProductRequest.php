<?php

namespace WBuilder\Core\Messages;

use PhpParser\JsonDecoder;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\Template;

class GetProductRequest extends AbstractRequest
{
    protected Product $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/get', 'GET');
        return $data;
    }

    public function parseData(){
        $products = collect(array());
        if($this->draft){
            $products = $this->draft->meta_data->meta->search('products', 'meta_key');
        }else{
            $products = $this->template->meta->search('products', 'meta_key');
        }
        if($products){
            return $products->data->collect()->firstWhere('id', '=',$this->getParameter('id'));
        }
        return null;

    }

}
