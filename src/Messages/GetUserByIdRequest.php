<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Customer;
use WBuilder\Core\Types\ListOfArticles;

class GetUserByIdRequest extends AbstractRequest
{
    protected Customer $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/retrieveById', 'GET');
        return $data;
    }

}
