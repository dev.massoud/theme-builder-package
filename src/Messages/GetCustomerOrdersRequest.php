<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\OrderPaginate;
use WBuilder\Core\Types\ListOfArticles;

class GetCustomerOrdersRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected OrderPaginate $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/orders', 'GET');
        return $data;
    }

}
