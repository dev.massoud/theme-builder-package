<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Types\ListOfArticles;
use WBuilder\Core\Types\ListOfProduct;

class GetProductGroupDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ProductPaginate $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/getByMetaKey', 'GET');
        return $data;
    }

    public function parseData(){
        $meta = $this->template->meta->collect();
        if($this->draft)
            $meta = $this->draft->meta_data->meta->collect();
        $records = $meta->where('meta_key', '=', $this->getParameter('meta_key'))->where('meta_type', '=', 'product_list')->first();
        $data = builder_pagination();
        if($records){
            $data = builder_pagination($records->data, count($records->data), $this->getParameter('limit'));
        }
        $products = [];
        foreach ($data->items() as $item){
            $product = $this->productById($item);
            if($product)
                $products[] = $product;
        }

        $to = $data->currentPage() * $data->perPage();
        if($to > $data->total()) $to = $data->total();
        $output['current_page'] = $data->currentPage();
        $output['from'] = (($data->currentPage() - 1) * $data->perPage()) + 1;
        $output['last_page'] = $data->lastPage();
        $output['per_page'] = $data->perPage();
        $output['to'] = $to;
        $output['total'] = $data->total();
        $output['data'] = $products;
        return new ProductPaginate($output);
    }

    protected function productById($id)
    {
        $products = collect(array());
        if($this->draft){
            $products = $this->draft->meta_data->meta->search('products', 'meta_key');
        }else{
            $products = $this->template->meta->search('products', 'meta_key');
        }

        if($products){
            return $products->data->collect()->firstWhere('id', '=',$id);
        }
        return null;
    }
}
