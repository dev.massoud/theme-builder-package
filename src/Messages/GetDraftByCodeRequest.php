<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Draft;

class GetDraftByCodeRequest extends AbstractRequest
{
    protected Draft $model;
    public function getData()
    {
        $data = $this->getBaseData('/drafts/get', 'GET');
        return $data;
    }

}
