<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class GetListOfProductColorsRequest extends AbstractRequest
{
    protected $data_type = 'list';
    protected ProductColor $model;

    public function getData()
    {
        $data = $this->getBaseData('/products/colors', 'GET');
        return $data;
    }

    public function parseData(){
        $products = new ListOfProduct();
        $colors = new ListOfProductColor();
        if($this->draft){
            $products = $this->draft->meta_data->meta->search('products', 'meta_key')->data;
        }else{
            $products = $this->template->meta->search('products', 'meta_key')->data;
        }
        /** @var Product $product */
        foreach ($products->all() as $product){
            if($product->colors->count() > 0){
                foreach ($product->colors->all() as $color){
                    if(!$colors->search($color->id))
                        $colors->insert($color);
                }
            }
        }
        return $colors;
    }
}
