<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Article;
use WBuilder\Core\Models\ArticlePaginate;
use WBuilder\Core\Types\ListOfArticles;

class GetArticlesDataRequest extends AbstractRequest
{
    protected $data_type = 'paginate';
    protected ArticlePaginate $model;

    public function getData()
    {
        $data = $this->getBaseData('/articles/getByMetaKey', 'GET');
        return $data;
    }

    public function parseData(){
        $meta = $this->template->meta->collect();
        if($this->draft)
            $meta = $this->draft->meta_data->meta->collect();
        $records = $meta->firstWhere('meta_key', '=', $this->getParameter('meta_key'));
        $data = builder_pagination();
        if($records){
            if(!is_array($records->data) && method_exists($records->data, 'all'))
                $data = builder_pagination($records->data->all(), $records->data->count(), $this->getParameter('limit'));
            else
                $data = builder_pagination($records->data, count($records->data), $this->getParameter('limit'));
        }

        $to = $data->currentPage() * $data->perPage();
        if($to > $data->total()) $to = $data->total();
        $output['current_page'] = $data->currentPage();
        $output['from'] = (($data->currentPage() - 1) * $data->perPage()) + 1;
        $output['last_page'] = $data->lastPage();
        $output['per_page'] = $data->perPage();
        $output['to'] = $to;
        $output['total'] = $data->total();
        $output['data'] = $data->items();
        return new ArticlePaginate($output);
    }
}
