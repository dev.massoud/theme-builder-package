<?php
namespace WBuilder\Core\Messages;

use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\StreamWrapper;
use http\Client\Request;
use WBuilder\Core\Enums\Mode;

abstract class AbstractRequest extends \WBuilder\Core\Common\Messages\AbstractRequest
{
    const API_VERSION = '1.0';

    protected $liveEndpoint = 'http://localhost/api/master/v1';
    protected $testEndpoint = 'http://51.79.73.93:8080/api/master/v1';
    protected $method;
    protected $endpoint;

    protected function getBaseData($action, $method = 'POST')
    {
        $data = array();
        $data['VERSION'] = static::API_VERSION;
        $this->method = $method;
        $this->endpoint = $this->getEndpoint().$action;
        return $data;
    }


    public function sendData($data)
    {

        $headers = [];
        $headers['x-api-key'] = $this->getAuthorizeKey();
        $parameters = $this->getParameters();
        unset($parameters['authorize_key']);
        unset($parameters['mode']);
        unset($parameters['preview_mode']);
        if($this->method == "GET"){
            $this->endpoint .= "?" . http_build_query($parameters, '', '&');
            $httpResponse = $this->httpClient->request($this->method, $this->endpoint, $headers);
        }else{
            $body = $this->toJSON($parameters);
            $headers['Accept'] = 'application/json';
            $headers['Content-type'] = 'application/json';
            $httpResponse = $this->httpClient->request($this->method, $this->endpoint, $headers, $body);
        }

        return $this->createResponse($httpResponse->getBody()->getContents());
    }
    public function toJSON($data, $options = 0)
    {
        // Because of PHP Version 5.3, we cannot use JSON_UNESCAPED_SLASHES option
        // Instead we would use the str_replace command for now.
        // TODO: Replace this code with return json_encode($this->toArray(), $options | 64); once we support PHP >= 5.4
        if (version_compare(phpversion(), '5.4.0', '>=') === true) {
            return json_encode($data, $options | 64);
        }
        return str_replace('\\/', '/', json_encode($data, $options));
    }

    protected function getEndpoint()
    {
        return $this->getMode() == "test" ? $this->testEndpoint : $this->liveEndpoint;
    }

    protected function createResponse($data)
    {
        return $this->response = new Response($this, $data);
    }

}
