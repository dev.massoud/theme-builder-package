<?php

namespace WBuilder\Core\Messages;

use WBuilder\Core\Models\Address;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;

class DeleteAddressRequest extends AbstractRequest
{
    protected Address $model;

    public function getData()
    {
        $data = $this->getBaseData('/customers/addresses/delete', 'GET');
        return $data;
    }
}
