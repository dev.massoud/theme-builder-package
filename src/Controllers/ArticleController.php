<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function index(){
        return view('builder/index');
    }
}
