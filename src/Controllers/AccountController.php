<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function profile(Request $request){
        $countries = builder()->countries();
        if($request->method() == "POST"){
            $validator = Validator::make($request->all(), [
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
            ]);
            if ($validator->fails())
                return back()->withErrors($validator)->withInput()->with('form', 'profile');

            try{
                $customer = builder()->customerUpdate(Auth::user()->id,
                    $request->get('firstname'),
                    $request->get('lastname'),
                    $request->get('gender'),
                    $request->get('phone_number'));

                return back()->withInput()->with('success',true)->with('form', 'profile');
            }catch (\Exception $exception){

            }

        }
        return view('builder/profile/index', compact('countries'));
    }

    public function profileAddress(Request $request, $type){
        $countries = builder()->countries();
        return view('builder/profile/address', compact('countries'))->with('form', 'address');
    }

    public function newProfileAddress(Request $request, $type){
        $countries = builder()->countries();
        $states = null;
        $cities = null;
        $address_info = null;
        if($request->get('id')) {
            $address_info = builder()->retrieveByAddressId($request->get('id'));
            if(!$address_info) abort(404);
            $states = builder()->states($address_info->country->id);
            $cities = builder()->cities($address_info->state->id);
        }
        if($request->method() == "POST"){
            $states = builder()->states($request->get('country'));
            $cities = builder()->cities($request->get('state'));

            try{
                $success_message = lang('content.new.address.created');
                if($request->has('id')){
                    $address = builder()->updateAddress($request->get('id'),
                        $request->get('firstname'),
                        $request->get('lastname'),
                        $request->get('email'),
                        $request->get('title'),
                        $request->get('country'),
                        $request->get('state'),
                        $request->get('city'),
                        $request->get('postal_code'),
                        $request->get('address')
                    );
                    $success_message = lang('content.address.updated');
                }else{
                    $address = builder()->createNewAddress(Auth::user()->getAuthIdentifier(),
                        $request->route('type'),
                        $request->get('firstname'),
                        $request->get('lastname'),
                        $request->get('email'),
                        $request->get('title'),
                        $request->get('country'),
                        $request->get('state'),
                        $request->get('city'),
                        $request->get('postal_code'),
                        $request->get('address')
                    );
                }
                return back()->with('success', $success_message)->with('form', 'address');
            }catch (\Exception $exception){
                $errors = json_decode($exception->getMessage(), true);
                return back()->withErrors($errors)->withInput()
                    ->with('form', 'address')
                    ->with("states", $states)
                    ->with("cities", $cities);
            }
        }
        return view("builder/profile/$type/new", array(
            'countries' => $countries,
            'states' => $states,
            'cities' => $cities,
            'address_info' => $address_info
        ))->with('form', 'address');
    }

    public function deleteAddress(Request $request, $id){
        try{
            $address = builder()->retrieveByAddressId($id);
            builder()->deleteAddress($id);
            return back()->withInput();
        }catch (\Exception $exception){
            abort(404);
        }

    }

    public function orders(Request $request){
        $per_page = 10;
        $page = ($request->get('page'))?:1;

        $orders = builder()->orders($per_page, $page, Auth::user()->getAuthIdentifier());
        return view('builder/profile/orders', array(
            'orders' => $orders
        ))->with('form', 'orders');
    }

    public function wishlist(Request $request){
        $per_page = 10;
        $page = ($request->get('page'))?:1;

        $products = builder()->wishlist($per_page, $page, Auth::user()->getAuthIdentifier());

        return view('builder/wishlist', array(
            'products' => $products
        ));
    }



}
