<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function states(Request $request){
        $states = builder()->states($request->get('country_id'));
        return response()->json($states, 200);
    }
    public function cities(Request $request){
        $cities = builder()->cities($request->get('state_id'));
        return response()->json($cities, 200);
    }

    public function productDetails(Request $request){
        $product = builder()->product($request->get('pid'));
        if(!$product) abort(404);

        return view('builder/ajax/'.$request->get('v'), compact('product'));
    }
}
