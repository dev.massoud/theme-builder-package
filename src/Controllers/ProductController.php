<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function product($title, $pid, Request $request){
        $product = builder()->product($pid);
        if(!$product) abort(404);
        $color = ($cid = $request->get("cid"))?(($product->colors->search($cid))?:abort(404)):null;
        $size = ($sid = $request->get("sid"))?(($product->sizes->search($sid))?:abort(404)):null;

        return view('builder/product-detail', compact('product', 'color', 'size'));
    }

    public function favorite(Request $request){
        if(!Auth::check()) abort(401);
        $favorite = builder()->favorite($request->get('product_id'), Auth::user()->getAuthIdentifier());
        if($request->get('json'))
            return response()->json($favorite);

        return back();
    }
}
