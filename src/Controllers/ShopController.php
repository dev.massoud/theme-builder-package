<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function shop(Request $request){
        $per_page = 12;
        $page = ($request->get('page'))?:1;
        $products = builder()
            ->products(
                $per_page,
                $page,
                $request->get('keyword'),
                explode(",", $request->get('c')),
                explode(",", $request->get('co')),
                explode(",", $request->get('s')),
                $request->get('sort_by'),
                $request->get('sort_dir')
            );
        return view('builder/shop', compact('products', 'per_page'));
    }
}
