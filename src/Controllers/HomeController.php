<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use WBuilder\Core\Builder;

class HomeController extends Controller
{
    public function index(Builder $builder){
        return view('builder/index');
    }
}
