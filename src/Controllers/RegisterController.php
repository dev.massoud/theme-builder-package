<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Shipu\Themevel\Facades\Theme;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('profile', route_params());
        $this->middleware('web');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     */
    protected function create($data)
    {
        $gender = isset($data['gender'])?:null;
        $phone_number = isset($data['phone_number'])?:null;
        return builder()->register($data['email'], $data['firstname'], $data['lastname'], $data['password'], $gender, $phone_number);

    }

    public function showRegistrationForm(Request $request)
    {
        return view('builder/auth/register');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $errors = [];
        try {
            event(new Registered($user = $this->create($request->all())));
            $this->guard()->login($user);

            if ($response = $this->registered($request, $user)) {
                return $response;
            }
            return $request->wantsJson()
                ? new JsonResponse([], 201)
                : redirect(route('profile', route_params()));

        }catch (\Exception $exception){
            $errors = json_decode($exception->getMessage(), true);
        }

        return $request->wantsJson()
            ? new JsonResponse(['errors' => $errors], 400)
            : redirect()->back()->withInput()->withErrors($errors);


    }
}
