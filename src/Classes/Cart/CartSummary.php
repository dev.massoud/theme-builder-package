<?php


namespace WBuilder\Core\Classes\Cart;

use WBuilder\Core\Models\Model;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;

class CartSummary
{
    protected int $count;
    protected int $sub_total;
    protected int $discount;
    protected int $tax;
    protected int $shipping;
    protected int $total;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getSubTotal(): int
    {
        return $this->sub_total;
    }

    /**
     * @param int $sub_total
     */
    public function setSubTotal(int $sub_total): void
    {
        $this->sub_total = $sub_total;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount(int $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return int
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * @param int $tax
     */
    public function setTax(int $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return int
     */
    public function getShipping(): int
    {
        return $this->shipping;
    }

    /**
     * @param int $shipping
     */
    public function setShipping(int $shipping): void
    {
        $this->shipping = $shipping;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }


    public function toJson(){
        return json_encode(array(
            "count" => $this->count,
            "sub_total" => $this->sub_total,
            "discount" => $this->discount,
            "shipping" => $this->shipping,
            "tax" => $this->tax,
            "total" => $this->total,
        ));
    }


}
