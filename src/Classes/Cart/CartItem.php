<?php


namespace WBuilder\Core\Classes\Cart;

use Illuminate\Support\Str;
use WBuilder\Core\Models\Model;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;

class CartItem
{
    public string $id;
    public Product $product;
    public ?ProductColor $color;
    public ?int $quantity;

    /**
     * CartItem constructor.
     * @param Product $product
     * @param ProductColor $color
     * @param int $quantity
     */
    public function __construct(Product $product, ?ProductColor $color, ?int $quantity = null)
    {
        $this->id = Str::uuid();
        $this->product = $product;
        $this->color = $color;
        $this->quantity = $quantity;
    }

    function toJson() {
        return json_encode(array(
            "id" => $this->id,
            "product" => ($this->product)?$this->product->toJson():null,
            "color" => ($this->color)?$this->color->toJson():null,
            "quantity" => $this->quantity
        ));
    }

}
