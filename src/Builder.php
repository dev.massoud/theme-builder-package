<?php


namespace WBuilder\Core;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use WBuilder\Core\Classes\Cart\Cart;
use WBuilder\Core\Common\Http\ClientInterface;
use WBuilder\Core\Common\Messages\RequestInterface;
use WBuilder\Core\Common\Messages\ResponseInterface;
use WBuilder\Core\Enums\Mode;
use WBuilder\Core\Models\Address;
use WBuilder\Core\Models\ArticlePaginate;
use WBuilder\Core\Models\Category;
use WBuilder\Core\Models\City;
use WBuilder\Core\Models\Country;
use WBuilder\Core\Models\Currency;
use WBuilder\Core\Models\Customer;
use WBuilder\Core\Models\Draft;
use WBuilder\Core\Models\FavoriteAction;
use WBuilder\Core\Models\Language;
use WBuilder\Core\Models\Meta;
use WBuilder\Core\Models\OrderPaginate;
use WBuilder\Core\Models\Paginate;
use WBuilder\Core\Models\Product;
use WBuilder\Core\Models\ProductColor;
use WBuilder\Core\Models\ProductFavorite;
use WBuilder\Core\Models\ProductPaginate;
use WBuilder\Core\Models\Service;
use WBuilder\Core\Models\State;
use WBuilder\Core\Models\Template;
use WBuilder\Core\Types\ListOfCategories;
use WBuilder\Core\Types\ListOfProduct;
use WBuilder\Core\Types\ListOfProductColor;
use WBuilder\Core\Types\ListOfProductSize;
use WBuilder\Core\Types\ListOfService;

class Builder extends AbstractBuilder
{
    public function getName()
    {
        return "Website Builder Gateway";
    }
    public function getDefaultParameters()
    {
        return array(
            'authorize_key' => config("website-builder.authorize_key") ?: "key",
            'mode' => config("website-builder.mode")?: "test",
            "preview_mode" => config("website-builder.preview_mode") ?: false
        );
    }
    public function getPreviewMode()
    {
        return $this->getParameter('preview_mode');
    }

    public function setPreviewMode($value)
    {
        return $this->setParameter('preview_mode', $value);
    }
    public function getAuthorizeKey()
    {
        return $this->getParameter('authorize_key');
    }

    public function setAuthorizeKey($value)
    {
        return $this->setParameter('authorize_key', $value);
    }

    public function getMode()
    {
        return $this->getParameter('mode');
    }

    public function setMode($value)
    {
        return $this->setParameter('mode', $value);
    }
    public function getIsDraft()
    {
        return $this->getParameter('is_draft');
    }

    public function setIsDraft($value)
    {
        return $this->setParameter('is_draft', $value);
    }

    /**
     * Get meta details
     *
     * @param $meta_key //group name or meta_key for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @return Paginate
     */
    public function meta($meta_key, $page = 1, $limit = 10)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetMetaDataRequest', array(
            "meta_key" => $meta_key,
            'page' => $page,
            "limit" => $limit
        ))->send();
    }

    /**
     * Get articles by special meta_key or group
     *
     * @param $meta_key //group name or meta_key for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @return ArticlePaginate
     */
    public function articles($meta_key, $page = 1, $limit = 10)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetArticlesDataRequest', array(
            "meta_key" => $meta_key,
            'page' => $page,
            "limit" => $limit
        ))->send();
    }

    /**
     * Get products by special group
     *
     * @param $group_name //group name for fetch data
     * @param $page // number of page
     * @param $limit // limit of data
     * @return ProductPaginate
     */
    public function productByGroup($group_name, $page = 1, $limit = 10)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetProductGroupDataRequest', array(
            "meta_key" => $group_name,
            'page' => $page,
            "limit" => $limit
        ))->send();
    }

    /**
     * Get Template Details
     *
     * @return Template
     */
    public function template() : ?Template
    {
        return $this->template;
    }

    /**
     * Get Website Draft Details
     *
     * @return Draft
     */
    public function draft() : ?Draft
    {
        return $this->draft;
    }

    public function key($meta_key, $index = null){
        $find = $this->template->meta->search($meta_key, 'meta_key');
        if($find)
            return $find->data;

        return $find;
    }

    /**
     * Translate text by default language
     *
     * @param string $value
     * @return string
     */
    public function translation($value){
        $language = (session()->has('language'))?session()->get('language'):$this->template->config['default_language'];
        if(key_exists($language, $value))
            return $value[$language];
        if(key_exists($this->template->config['default_language'], $value))
            return $value[$this->template->config['default_language']];
        return null;
    }

    /**
     * Get List Of Languages
     *
     * @return Language[]
     */
    public function languages()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetLanguagesRequest')->send();
    }

    /**
     * Get List Of Currencies
     *
     * @return Currency[]
     */
    public function currencies()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetCurrenciesRequest')->send();
    }

    /**
     * Get List Of Countries
     *
     * @return Country[]
     */
    public function countries()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetCountriesRequest')->send();
    }

    /**
     * Get List Of States
     *
     * @return State[]
     */
    public function states($country_id = null)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetStatesRequest', array(
            "country_id" => $country_id
        ))->send();
    }

    /**
     * Get List Of Cities
     *
     * @return City[]
     */
    public function cities($state_id = null, $country_id = null)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetCitiesRequest', array(
            "country_id" => $country_id,
            "state_id" => $state_id,
        ))->send();
    }

    /**
     * Get Product Details By Id
     *
     * @param int $id
     * @return Product
     */
    public function product($id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetProductRequest', array(
            'id' => $id
        ))->send();
    }

    /**
     * Get List Of categories
     *
     * @return ?ListOfCategories
     */
    public function categories()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetListOfCategoriesRequest')->send();
    }

    /**
     * Get List Of product colors
     *
     * @return ?ListOfProductColor
     */
    public function colors()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetListOfProductColorsRequest')->send();
    }

    /**
     * Get List Of product sizes
     *
     * @return ?ListOfProductSize
     */
    public function sizes()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetListOfProductSizesRequest')->send();
    }

    /**
     * Search in products list
     * @param int $limit
     * @param int $page
     * @param string $keyword
     * @param array $categories
     * @param array $colors
     * @param array $sizes
     * @param string $sort_field
     * @param string $sort_type
     * @return ProductPaginate
     * @throws Common\Exception\InvalidRequestException
     */
    public function products($limit, $page, $keyword = "", $categories = [], $colors = [], $sizes = [], $sort_field = null, $sort_type = null)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetProductsDataRequest', array(
            'limit' => $limit,
            'page' => $page,
            'keyword' => $keyword,
            'categories' => implode(",", $categories),
            'colors' => implode(",", $colors),
            'sizes' => implode(",", $sizes),
            'sort_field' => $sort_field,
            'sort_type' => $sort_type,
        ))->send();
    }

    /**
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $password
     * @param $gender
     * @param $phone_number
     * @return ?Customer
     */
    public function register($email, $firstname, $lastname, $password, $gender, $phone_number)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CustomerRegisterRequest', array(
            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password,
            'gender' => $gender,
            'phone_number' => $phone_number,
        ))->send();
    }

    /**
     * @param $email
     * @param $password
     * @return ?Customer
     */
    public function login($email, $password)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CustomerLoginRequest', array(
            'email' => $email,
            'password' => $password
        ))->send();
    }

    /**
     * @param $id
     * @return ?Customer
     */
    public function retrieveByUserId($id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetUserByIdRequest', array(
            'id' => $id
        ))->send();
    }

    /**
     * update customer information
     * @param $id
     * @param $firstname
     * @param $lastname
     * @param $gender
     * @param $phone_number
     * @return ?Customer
     */
    public function customerUpdate($id, $firstname, $lastname, $gender, $phone_number)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CustomerUpdateRequest', array(
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'gender' => $gender,
            'phone_number' => $phone_number,
        ))->send();
    }

    /**
     * Retrieve Address Information By Id
     *
     * @param $id
     * @return ?Address
     */
    public function retrieveByAddressId($id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetAddressByIdRequest', array(
            'id' => $id
        ))->send();
    }

    /**
     * Create New Address For Customers
     *
     * @param $customer_id
     * @param $type
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $title
     * @param $country_id
     * @param $state_id
     * @param $city_id
     * @param $postal_code
     * @param $address
     * @return ?Address
     */
    public function createNewAddress($customer_id, $type, $firstname, $lastname, $email, $title, $country_id, $state_id, $city_id, $postal_code, $address)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CreateAddressRequest', array(
            'customer_id' => $customer_id,
            'type' => $type,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'title' => $title,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'postal_code' => $postal_code,
            'address' => $address,
        ))->send();
    }

    /**
     * Update Address For Customers BY Id
     *
     * @param $id
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $title
     * @param $country_id
     * @param $state_id
     * @param $city_id
     * @param $postal_code
     * @param $address
     * @return ?Address
     */
    public function updateAddress($id, $firstname, $lastname, $email, $title, $country_id, $state_id, $city_id, $postal_code, $address)
    {
        return $this->createRequest('\WBuilder\Core\Messages\UpdateAddressRequest', array(
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'title' => $title,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'postal_code' => $postal_code,
            'address' => $address,
        ))->send();
    }

    /**
     * Delete Customer Address
     *
     * @param $id
     * @return ?array
     */
    public function deleteAddress($id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\DeleteAddressRequest', array(
            'id' => $id
        ))->send();
    }

    /**
     * Get List Of Customer Orders
     *
     * @param $limit
     * @param $page
     * @param $customer_id
     * @return ?OrderPaginate
     */
    public function orders($limit, $page, $customer_id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetCustomerOrdersRequest', array(
            'customer_id' => $customer_id,
            'limit' => $limit,
            'page' => $page,
        ))->send();
    }

    /**
     * Get List Of Customer Wisheslist
     *
     * @param $limit
     * @param $page
     * @param $customer_id
     * @return ?ProductPaginate
     */
    public function wishlist($limit, $page, $customer_id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetCustomerWishlistRequest', array(
            'customer_id' => $customer_id,
            'limit' => $limit,
            'page' => $page,
        ))->send();
    }

    /**
     * Add to favorite products
     *
     * @param $customer_id
     * @param $product_id
     * @return ?FavoriteAction
     */
    public function favorite($product_id, $customer_id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\FavoriteRequest', array(
            'customer_id' => $customer_id,
            'id' => $product_id
        ))->send();
    }

    /**
     * Check favorite products
     *
     * @param $customer_id
     * @param $product_id
     * @return ?ProductFavorite
     */
    public function checkFavorite($product_id, $customer_id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CheckFavoriteRequest', array(
            'customer_id' => $customer_id,
            'id' => $product_id
        ))->send();
    }

    /**
     * Get list of payment services
     *
     * @return ?ListOfService
     */
    public function payments()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetPaymentServicesRequest')->send();
    }

    /**
     * Get list of delivery services
     *
     * @return ?ListOfService
     */
    public function deliveries()
    {
        return $this->createRequest('\WBuilder\Core\Messages\GetDeliveryServicesRequest')->send();
    }

    /**
     * Get list of delivery services
     *
     */
    public function createOrder($customer_id, $shipping_address, $billing_address, $shipping_service_id, $shipping_sevice_rate_id, $currency_id)
    {
        return $this->createRequest('\WBuilder\Core\Messages\CreateOrderRequest', array(
            "customer_id" => $customer_id,
            "shipping_address" => json_encode($shipping_address),
            "billing_address" => json_encode($billing_address),
            'shipping_service_id' => $shipping_service_id,
            'shipping_sevice_rate_id' => $shipping_sevice_rate_id,
            'cart' => $this->cart()->get()->toJson(),
            'cart_summary' => $this->cart()->summary()->toJson(),
            'currency_id' => $currency_id
        ))->send();
    }

    /**
     * Get Cart
     *
     * @return Cart
     */
    public function cart()
    {
        return $this->cart;
    }

    /**
     * Get default language
     *
     * @return Language
     */
    public function getDefaultLanguage()
    {
        if($this->draft())
            return $this->draft()->meta_data->languages[0];

        $languages = $this->languages();
        if(count($languages) > 0)
            return $languages[0];
        return null;
    }

    /**
     * Get default currency
     *
     * @return Currency
     */
    public function getDefaultCurrency()
    {
        if($this->draft())
            return $this->draft()->meta_data->currencies[0];

        $currencies = $this->currencies();
        if(count($currencies) > 0)
            return $currencies[0];
        return null;
    }


}
