<?php

use Illuminate\Pagination\LengthAwarePaginator;

function startsWith ($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}
function route_params($params = []) {
    if(request()->has('template_id'))
        $params['template_id'] = request()->get('template_id');
    if(request()->has('code'))
        $params['code'] = request()->get('code');

    return $params;
}

function builder_image_url($image, $default = null){
    if(!$image && !$default) return null;
    if(!$image && $default) return builder_image_url($default);

    if(startsWith($image, 'http')) return $image;
    if(startsWith($image, 'data:')) return $image;
    return asset("assets/$image");
}

function builder_resource_url($resource){
    if(startsWith($resource, 'http')) return $resource;
    if(startsWith($resource, 'data:')) return $resource;
    return asset("assets/$resource");
}

function builder_translate($translations, $key){
    $language = (session()->has('language'))?session()->get('language'):false;
    $translations = collect($translations);
    if($language){
        $content = $translations->firstWhere('id', '=', $language);
        if(key_exists($key, $content))
            return $content[$key];
    }
    if($translations->count() > 0){
        $content = $translations->first();
        if(key_exists($key, $content))
            return $content[$key];
    }
    return null;

}
function builder_translation($value){
    $language = (session()->has('language'))?session()->get('language'):false;
    if(key_exists($language, $value))
        return $value[$language];
    if(count(array_keys($value)) > 0){
        return $value[array_keys($value)[0]];
    }
    return null;

}
function builder_default_price($prices){
    $currency = (session()->has('currency'))?session()->get('currency'):false;
    $prices = collect($prices);
    if($currency){
        $price = $prices->firstWhere('currency.id', '=', $currency);
        if($price)
            return $price;
    }
    if($prices->count() > 0){
        $price = $prices->first();
        if($price)
            return $price[0];
    }
    return null;

}

function builder_model_of_data($data, $model){
    $output = [];
    foreach ($data as $item){
        $output[] = new $model($item);
    }
    return $output;
}

function builder_list_of_data($data, $list, $model){
    $output = new $list();
    foreach ($data as $item){
        $output->insert(new $model($item));
    }
    return $output;
}

function builder_put_data_to_list($data, $list){
    $output = new $list();
    foreach ($data as $item){
        $output->insert($item);
    }
    return $output;
}

/**
 * Builder class
 *
 * @return LengthAwarePaginator
 */
function builder_pagination($data = [], $count = 0, $limit = 10){
    return new LengthAwarePaginator(
        $data,
        $count,
        $limit,
        LengthAwarePaginator::resolveCurrentPage("page"),
        [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
            'pageName' => "page",
        ]
    );
}
/**
 * Builder class
 *
 * @return \WBuilder\Core\Builder
 */
function builder(){
    return app(\WBuilder\Core\Builder::class);
}

