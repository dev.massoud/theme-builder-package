<?php
namespace WBuilder\Core;

class RouteMethods
{
    public function load()
    {
        return function ($options = []) {
            $namespace = class_exists($this->prependGroupNamespace('Builder\HomeController')) ? null : 'App\Http\Controllers';
            $this->group(['namespace' => $namespace, 'middleware' => ['web']], function() use($options) {
                $this->get('/', 'Builder\HomeController@index')->name('web-home');
                $this->get('/profile', 'Builder\AccountController@profile')->middleware('auth:web')->name('profile');
                $this->post('/profile', 'Builder\AccountController@profile')->middleware('auth:web');
                $this->get('/wishlist', 'Builder\AccountController@wishlist')->middleware('auth:web')->name('wishlist');
                $this->get('/checkout', 'Builder\CartController@checkout')->middleware('auth:web')->name('checkout');
                $this->post('/checkout', 'Builder\CartController@checkout')->middleware('auth:web');
                $this->get('/address/{type}', 'Builder\AccountController@profileAddress')->middleware('auth:web')->name('profile-address');
                $this->get('/address/{type}/new', 'Builder\AccountController@newProfileAddress')->middleware('auth:web')->name('new-profile-address');
                $this->post('/address/{type}/new', 'Builder\AccountController@newProfileAddress')->middleware('auth:web');
                $this->get('/address/{type}/edit', 'Builder\AccountController@newProfileAddress')->middleware('auth:web')->name('edit-profile-address');
                $this->post('/address/{type}/edit', 'Builder\AccountController@newProfileAddress')->middleware('auth:web');
                $this->get('/address/delete/{id}', 'Builder\AccountController@deleteAddress')->middleware('auth:web')->name('profile-address-delete');
                $this->get('/profile/orders', 'Builder\AccountController@orders')->middleware('auth:web')->name('profile-orders');
                $this->post('/product/favorite/add', "Builder\ProductController@favorite")->middleware('auth:web')->name('add-to-favorite');
                $this->group(['prefix' => '/cart'], function() use($options) {
                    $this->get('/', 'Builder\CartController@cart')->name('cart');
                    $this->post('/add', 'Builder\CartController@addToCart')->name('add-to-cart');
                    $this->post('/delete', 'Builder\CartController@deleteCartItem')->name('delete-cart-item');
                });
                $this->get('/shop', 'Builder\ShopController@shop')->name('shop');
                $this->get('/product/{title}/{pid}', 'Builder\ProductController@product')->name('product');
                $this->get('/articles', 'Builder\ArticleController@index')->name('articles-list');
                $this->get('/article/{title}/{id}', 'Builder\ArticleController@article')->name('article');
                $this->group(['prefix' => '/ajax'], function() use($options) {
                    $this->get('/product-details', 'Builder\AjaxController@productDetails')->name('ajax-product-details');
                    $this->post('/states', "Builder\AjaxController@states")->name('get-states');
                    $this->post('/cities', "Builder\AjaxController@cities")->name('get-cities');
                    $this->post('/template/product-items', "Builder\AjaxController@generateProductTemplate")->name('generate-product-list');
                    $this->post('/template/shop', "Builder\AjaxController@generateShopTemplate")->name('generate-shop');
                    $this->post('/template/product-colors', "Builder\AjaxController@generateProductColorsTemplate")->name('generate-product-colors');
                    $this->post('/template/product-sizes', "Builder\AjaxController@generateProductSizesTemplate")->name('generate-product-sizes');
                });


                $this->get('/login', "Builder\LoginController@showLoginForm")->name('login');
                $this->post('/login', "Builder\LoginController@login");
                $this->get('/register', "Builder\RegisterController@showRegistrationForm")->name('register');
                $this->post('/register', "Builder\RegisterController@register");
                $this->get('/password/reset', "Builder\ForgotPasswordController@showLinkRequestForm")->name('password.request');
                $this->post('/password/reset', "Builder\ForgotPasswordController@reset")->name('password.update');
                $this->post('/password/email', "Builder\ForgotPasswordController@sendResetLinkEmail")->name('password.email');
                $this->get('/password/reset/{token}', "Builder\ForgotPasswordController@showResetForm")->name('password.reset');
                $this->get('/password/confirm', "Builder\ForgotPasswordController@showConfirmForm")->name('password.confirm');
                $this->post('/password/confirm', "Builder\ForgotPasswordController@confirm");
                $this->post('/logout', "Builder\LoginController@logout")->name('logout');

            });
        };
    }


}

